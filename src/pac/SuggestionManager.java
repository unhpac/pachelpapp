package pac;

import java.util.ArrayList;

public class SuggestionManager {

  private ArrayList<Suggestion> suggestions = null;

  public SuggestionManager() {
    suggestions = new ArrayList<Suggestion>();
  }

  public void add(final String suggestion, final String consultant, final String dateTime) {
    suggestions.add(new Suggestion(suggestion, consultant, dateTime));
  }

  public boolean loadFromString(final String fullSuggestionString) {
    return true;
  }

  public Suggestion get(final int i) {
    return suggestions.get(i);
  }

  public int size() {
    return suggestions.size();
  }

  @Override
  public String toString() {
    return "No suggestions available.";
  }

  public class Suggestion {
    public String suggestion, consultant, dateTime;

    public Suggestion(final String suggestion, final String consultant, final String dateTime) {
      this.suggestion = suggestion;
      this.consultant = consultant;
      this.dateTime = dateTime;
    }
  }

}
