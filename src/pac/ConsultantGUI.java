/**
 * This class creates the GUI used by the help application to send claims.
 * 
 * @author Stephen Dunn
 */
package pac;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.RowFilter;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableRowSorter;

import pac.Packet.PACKET;
import pac.Utils.CONFIG_VAR;

public class ConsultantGUI extends JFrame implements ItemListener {
  /**
   * 
   */
  private static final long    serialVersionUID     = 4132618850192464149L;
  private Client               client;
  private ClaimManager         claims;

  private final int            admin                = 0;
  private int                  port                 = -1;
  private String               username             = null;
  private String               computer             = null;
  private String               consultant           = null;
  private String               server               = null;
  private String[]             args;

  private final String         MSG_LOST_CONNECTION  = "I've lost communication with the server. "
      + "Will automatically reconnect when the server is back online.";

  // GUI Components *************************

  // font
  private Font                 font = null;
  private final String         FONT_PATH            = "resources/font.ttf";

  // icon
  private ImageIcon            icon;
  private final String         ICON_PATH            = "resources/Icon.gif";

  // buttons & boxes
  private final String[]       buttonLabels         = { "Send Help",
      "Fullscreen Selection", "Common Problem List" };
  private final int            BTN_SEND_INDEX       = 0, BTN_FULLSCREEN_INDEX = 1, BTN_COMMONLIST_INDEX = 2;
  private JButton[]            button;
  private JCheckBox[]          checkBoxes;

  // menus
  private final String[]       menuLabels           = { "File", "Help" };
  private final int            FILE_INDEX           = 0, HELP_INDEX = 1, LOGIN_INDEX = 0,
      QUIT_INDEX = 1, ABOUT_INDEX = 0;

  public String[]           columnNames      = { "Wait Time", "User", "Name", "Machine",
      "Course(s)", "Problem" };
  private final int            TIME_COL = 0, USER_COL = 1, NAME_COL = 2, COMP_COL = 3,
      COURSE_COL = 4, STATE_COL = 5, CLAIM_COL = 6;
  private JMenuBar             menuBar;
  private JMenu[]              menus;
  private ArrayList<JMenuItem> menuItems;

  // panels & panes
  private final String         TXT_DEFAULT          = "Select a claim to view.";
  private final String         TXT_SELECT_CLAIM     = "Select a claim to view.";
  private final String         TXT_NO_CLAIMS        = "No claims are currently available.";
  private JPanel               centerPanel, leftPanel, rightPanel;
  private JScrollPane          currentScrollPane, scrollTablePane;
  private ProblemViewerFrame   currentProblemWindow = null;

  // tables
  private final int             DEFAULT_WIDTH        = 1100, DEFAULT_HEIGHT = 600;
  private int                   selectedRow = -1, filterCount = 0;
  private String                filterString = null;
  RowFilter<ClaimsTableModel, Integer>  courseFilter = null;
  TableRowSorter<ClaimsTableModel>      sorter       = null;
  private ListSelectionListener selectionListener;
  private JTable                claimsTable;
  private ClaimsTableModel      claimsTableModel;
  private ClaimCellRenderer     claimsCellRenderer;
  private JTextArea             text;

  //for preserving sorts after resetTable()
  private int                  sortCount            = 2, lastSortedColumn = 0;

  // timers
  final private Timer          refreshTimer         = new Timer();

  public ConsultantGUI() throws NullPointerException {
    super("Consultant Claim Manager");
    super.setVisible(false);

    if (!Utils.loadConfig()) Utils.updateConfigFile();

    if (!Log.createLog(Utils.getGUILog())) {
      Log.err("fatal error: cannot read/write a log file");
      throw new NullPointerException();
    }

    Log.out("PACConsultantGUI (v" + HelpApp.VERSION + ") initializing...");

    claims = new ClaimManager();
    claims.clear(); // for debug mode random claims

    if (!initGUI()) {
      Log.err("failed to build GUI components");
      throw new NullPointerException();
    }

    initInfo();

    if (!initNetwork()) Log.out("failed to connect to the server");

    // obtain heartbeat from config
    Integer refreshTimeInSeconds = null;
    try {
      refreshTimeInSeconds = Utils.getHeartbeat();
      if (refreshTimeInSeconds == null) {
        Log.err("bad heartbeat specfified in config file, reverting to default");
        Utils.setConfigValue(CONFIG_VAR.HEARTBEAT, Utils.getConfigDefaultValue(CONFIG_VAR.HEARTBEAT));
        refreshTimeInSeconds = Utils.getDefaultHeartbeat();
      }
    } catch (final Exception e) {
      Log.err("failed to obtain heartbeat value");
      throw new NullPointerException();
    }

    // schedule heartbeat/refresh/reconnect timer
    refreshTimer.schedule(new RefreshTimerListener(), 0, refreshTimeInSeconds * 1000);

    // enable password protected closing with debug mode off
    if (!Utils.isDebugMode()) {
      setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
      setUndecorated(true);
    } else addWindowListener(new java.awt.event.WindowAdapter() {
      @Override
      public void windowClosing(final WindowEvent winEvt) {
        exit();
      }
    });

    addWindowListener(new WindowAdapter() {
      @Override
      public void windowOpened(final WindowEvent e) {
        setExtendedState(Frame.MAXIMIZED_BOTH);
      }
    });

    Log.out("PACConsultantGUI initialized");
    focus();
  }

  public void focus() {
    setVisible(true);
    setLocationRelativeTo(null);
    setState(Frame.NORMAL);
    toFront();
  }

  public void exit() {
    exit(0);
  }

  public void exit(final int exitCode) {
    if (refreshTimer != null)
      refreshTimer.cancel();
    if (client != null)
      client.disconnect();

    if (exitCode == 0) Log.out("PACConsultantGUI terminated properly");
    else Log.err("PACConsultantGUI terminated with error code: " + exitCode);

    Log.closeLog();
    System.exit(exitCode);
  }

  public boolean initInfo() {
    // get computer name
    computer = Utils.getHostname();
    if (computer == null) {
      Log.err("failed to determine computer name");
      computer = Claim.UNKNOWN_VALUE;
    }

    // get some valid username
    username = Utils.getUsername();
    if (username == null) {
      username = Utils.promptForUsername();
      if (username == null) exit();
    }

    server = Utils.getClientServer();
    if (Utils.isLocalhostServer()) server = "127.0.0.1";
    port = Utils.getPort().intValue();

    return true;
  }

  public boolean initGUI() {

    try {
      icon = new ImageIcon(ICON_PATH, "PAC Icon");
      setIconImage(Toolkit.getDefaultToolkit().getImage(ICON_PATH));
    } catch (final Exception e) {
      try {
        setIconImage(ImageIO.read(HelpApp.class.getResourceAsStream("/" + ICON_PATH)));
      } catch (Exception e2) {
        Log.err("missing or corrupt icon file: " + ICON_PATH);
      }
    }

    setUndecorated(false);
    setVisible(false);
    setResizable(true);
    setLocationRelativeTo(getRootPane());

    button = new JButton[buttonLabels.length];

    // menu creation *****************************
    menuBar = new JMenuBar();

    menus = new JMenu[menuLabels.length];

    for (int i = 0; i < menuLabels.length; i++) {
      menus[i] = new JMenu(menuLabels[i]);
      menuBar.add(menus[i]);
    }

    // add menu items **********************************
    menuItems = new ArrayList<JMenuItem>();
    JMenuItem cur;

    cur = new JMenuItem("Consultant Login", KeyEvent.VK_L);
    cur.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L,
        ActionEvent.ALT_MASK));
    cur.getAccessibleContext().setAccessibleDescription(
        "Consultant Login");
    cur.addActionListener(new GUIMenuListener(this, FILE_INDEX, LOGIN_INDEX));

    menuItems.add(cur);
    menus[FILE_INDEX].add(cur);

    cur = new JMenuItem("Quit", KeyEvent.VK_Q);
    cur.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q,
        ActionEvent.ALT_MASK));
    cur.getAccessibleContext().setAccessibleDescription(
        "Exit Consultant GUI");
    cur.addActionListener(new GUIMenuListener(this, FILE_INDEX, QUIT_INDEX));

    menuItems.add(cur);
    menus[FILE_INDEX].addSeparator();
    menus[FILE_INDEX].add(cur);

    cur = new JMenuItem("About", KeyEvent.VK_A);
    cur.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A,
        ActionEvent.ALT_MASK));
    cur.getAccessibleContext().setAccessibleDescription(
        "About the Consultant GUI");
    cur.addActionListener(new GUIMenuListener(this, HELP_INDEX, ABOUT_INDEX));

    menuItems.add(cur);
    menus[HELP_INDEX].add(cur);

    setJMenuBar(menuBar);

    // center panel ***********************************
    centerPanel = new JPanel(new BorderLayout());

    final JPanel innerCenter = new JPanel(new GridLayout(2, 1));

    text = new JTextArea(TXT_DEFAULT, 50, 60);
    try {
      font = Font.createFont( 0, HelpApp.class.getResourceAsStream("/" + FONT_PATH) );
      Font useFont = font.deriveFont(0, 20);
      text.setFont(useFont);
    } catch (Exception e) {
      Log.err("failed to load font \"" + FONT_PATH + "\"");
    }
    text.setText(TXT_DEFAULT);

    // build the table
    buildTable();

    currentScrollPane = new JScrollPane(text);
    currentScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
    currentScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

    scrollTablePane = new JScrollPane(claimsTable);

    innerCenter.add(scrollTablePane);
    innerCenter.add(currentScrollPane);
    centerPanel.add(innerCenter, BorderLayout.CENTER);

    // right panel *************************************
    rightPanel = new JPanel(new GridLayout(button.length + 1, 1)); // +1 for checkbox panel

    // create checkboxes for filtering by class code
    final Panel filterPanel = new Panel();
    filterPanel.setLayout(new GridLayout(Utils.classCodes.length + 1, 1)); // +1 for the label
    filterPanel.add(new JLabel("Filter By Course:"));
    checkBoxes = new JCheckBox[Utils.classCodes.length];
    for (int i = 0; i < Utils.classCodes.length; i++) {
      checkBoxes[i] = new JCheckBox(Utils.classCodes[i]);
      checkBoxes[i].setSelected(true);
      checkBoxes[i].setOpaque(true);
      checkBoxes[i].setForeground(Color.BLACK);
      checkBoxes[i].setBackground(Utils.classColors[i % Utils.classColors.length]);
      checkBoxes[i].addItemListener(this);
      filterPanel.add(checkBoxes[i]);
    }
    rightPanel.add(filterPanel);

    // create buttons
    for (int i = 0; i < buttonLabels.length; i++) {
      button[i] = new JButton(buttonLabels[i]);
      button[i].setSize(100, 100);
      button[i].addActionListener(new GUIButtonListener(i));
      rightPanel.add(button[i], BorderLayout.CENTER);
    }

    button[BTN_SEND_INDEX].setEnabled(false); // send disabled for now

    // add panels to the JFrame
    add(centerPanel, BorderLayout.CENTER);
    add(rightPanel, BorderLayout.EAST);

    // create problem viewer frame for fullscreen
    currentProblemWindow = new ProblemViewerFrame();

    return true;
  }

  public boolean initNetwork() {
    try {
      client = new Client();
    } catch (final Exception e) {
      return false;
    }

    Log.out("connecting to \"" + server + ":" + port + "\" as \"" + username + "\"..." );
    // connect
    try {
      if (client.connect(server, port, username, computer, username) != 0) {
        Log.out("failed to communicate with server at \"" + client.getServer() + ":" + client.getPort() + "\"");
        return false;
      }
    } catch (final Exception e) {
      Log.err("internal connection failure for server: \""
          + client.getServer() + ":" + client.getPort() + "\"");
      return false;
    }

    // receive old claim (should be just a heartbeat on GUI connection)
    try {
      Packet p = client.receive();
      p = client.receive();
    } catch (final Exception e) {
      Log.err("connection to server \"" + client.getServer() + ":" + client.getPort() + "\" failed");
      return false;
    }

    return true;
  }

  public void sendHelp(final int claimId, final String username, final String suggestion) {
    client.sendSuggestion(claimId, consultant, username, suggestion);
  }

  public void refreshClaims() {
    if (!client.isConnected()) return;

    // request claims update
    final Packet getClaims = new Packet(PACKET.GET_CLAIMS);
    if (client.send(getClaims) != 0) {
      client = null;
      Log.msg(MSG_LOST_CONNECTION);
      return;
    }

    // receive update
    Packet allClaims = null;
    try {
      allClaims = client.receive();
    } catch (final Exception e) {
      client = null;
      Log.msg(MSG_LOST_CONNECTION);
      return;
    }

    if (allClaims.data == null) return;

    ClaimManager temp = new ClaimManager();
    temp.clear();

    if (allClaims.data.contains(ClaimManager.claimPartSeparatorSymbol)) {
      temp.loadFromString(allClaims.data);
    }

    synchronized(claims) {
      for (int i = 0; i < temp.size(); i++)
        claims.update(temp.get(i));
    }

    claimsTableModel.update();
  }

  // respond to the course filter checkboxes
  @Override
  public void itemStateChanged(final ItemEvent e) {
    filterString = getFilterString();

    if (sorter != null) {
      filterCount = 0;
      sorter.setRowFilter(null);
      sorter.setRowFilter(courseFilter);
    }
  }

  public String getFilterString() {
    if (checkBoxes == null) return null;

    String filter = "";
    for (JCheckBox checkBox : checkBoxes) {
      if (!checkBox.isSelected())
        filter += checkBox.getText();
    }
    return filter;
  }

  public void buildTable() {
    claimsTableModel = new ClaimsTableModel();
    claimsTable = new JTable(claimsTableModel);
    claimsCellRenderer = new ClaimCellRenderer();
    selectionListener = new ClaimSelectionListener(claimsTable);

    claimsTable.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    claimsTable.getSelectionModel().addListSelectionListener(selectionListener);

    claimsTable.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(final MouseEvent mouseEvent) {
        if (mouseEvent.getClickCount() == 2) fullscreenProblem();
      };
    });

    for (int i = 0; i < claimsTable.getColumnModel().getColumnCount(); i++)
      claimsTable.getColumnModel().getColumn(i).setCellRenderer(claimsCellRenderer);

    claimsTable.setFillsViewportHeight(true);
    claimsTable.setAutoCreateRowSorter(false);
    claimsTable.setRowHeight(25);
    claimsTable.setRowSelectionAllowed(true);
    claimsTable.setColumnSelectionAllowed(false);
    claimsTable.getColumnModel().getColumn(claimsTableModel.columnNames.length - 1).setPreferredWidth(600);

    // create a filter for the checkboxes
    try {
      courseFilter = new RowFilter<ClaimsTableModel, Integer>() {
        @Override
        public boolean include(Entry<? extends ClaimsTableModel, ? extends Integer> entry) {
          if (filterString == null) return true;

          String course = entry.getStringValue(COURSE_COL);
          if (course == null || course.length() < 1) return false;
          if (filterString.contains(course)) return false;

          filterCount++;

          return true;
        }
      };
    } catch (Exception e) {}

    sorter = new TableRowSorter<ClaimsTableModel>(claimsTableModel);
    claimsTable.setRowSorter(sorter);
  }

  public void clear() {
    if (claimsTable != null && claimsTable.getModel() != null)
      ((ClaimsTableModel)claimsTable.getModel()).clear();
  }

  public void fullscreenProblem() {
    if (claimsTable.getSelectedRow() < 0)
      return;

    try {

      final String course = (String)claimsTable.getValueAt(claimsTable.getSelectedRow(),
          ((ClaimsTableModel)claimsTable.getModel()).columnNames.length - 1);
      final String firstname = (String)claimsTable.getValueAt(claimsTable.getSelectedRow(), NAME_COL);
      final String waittime = (String)claimsTable.getValueAt(claimsTable.getSelectedRow(), TIME_COL);

      currentProblemWindow.open(firstname, course, waittime);

    } catch (Exception e) {
      Log.msg("You must select a claim.");
    }
  }

  public class RefreshTimerListener extends TimerTask {
    /**
     * Refresh/Heartbeat/Reconnect timer for GUI
     * 
     * @author Stephen Dunn
     */
    public RefreshTimerListener() {}

    @Override
    public void run() { // isConnected() sends a heartbeat
      if (client.isConnected()) refreshClaims(); // obtain new claims as packet
      else initNetwork();
    }
  }

  public class ClaimsTableModel extends AbstractTableModel {
    /**
     * 
     */
    private static final long serialVersionUID = 1620814617587877453L;
    private final Integer maxRow                 = new Integer(500);
    public String[]           columnNames      = { "Wait Time", "User", "Name", "Machine",
        "Course(s)", "State", "Problem" };
    private Object[][]        data             = { { null, null, null, null, null, null, null } };

    public ClaimsTableModel() {
      super();

      synchronized (maxRow) {
        data = new Object[maxRow][columnNames.length];
      }
    }

    public void update() {

      // request the current server time for calculating elapsed claim times
      Packet serverTime = new Packet(PACKET.DATE_TIME);
      String time = null, date = null;
      if (client.send(serverTime) != 0) return;
      try {
        serverTime = client.receive();
        final String[] parts = serverTime.getData().split(" ");
        time = parts[1];
        date = parts[0];
      } catch (final Exception e) {
        return;
      } finally {
        if (time == null) return;
      }

      if (claims == null) return;
      synchronized(claims) {
        for (int i = 0; i < claims.size(); i++) {
          Claim claim = claims.get(i);
          String paddedTime = Utils.fixTimeFormatting(Utils.timeElapsed(claim.timeStarted, time));
          if (!date.equals(claim.dateStarted)) paddedTime = "99:99:99";

          data[i][TIME_COL] = paddedTime;
          data[i][USER_COL] = claim.username;
          data[i][NAME_COL] = claim.firstName;
          data[i][COMP_COL] = claim.computer;
          data[i][COURSE_COL] = claim.groups;
          data[i][STATE_COL] = claim.state.name();
          data[i][CLAIM_COL] = claim.claim;
        }
      }

    }

    @Override
    public int getColumnCount() {
      if (columnNames == null)
        return 0;
      return columnNames.length;
    }

    @Override
    public int getRowCount() {
      if (data == null)
        return 0;
      return data.length;
    }

    @Override
    public String getColumnName(final int col) {
      return columnNames[col];
    }

    @Override
    public Object getValueAt(final int row, final int col) {
      synchronized (maxRow) {
        if (data == null)
          return null;
        try {
          final Object ret = data[row][col];
          return ret;
        } catch (final Exception e) {} // silence these exceptions for now
      }
      return null;
    }

    @Override
    public void setValueAt(final Object value, final int row, final int col) {
      synchronized (maxRow) {
        try {
          data[row][col] = value;
          fireTableCellUpdated(row, col);
        } catch (final Exception e) {}
      }
    }

    public void clear() {
      synchronized (maxRow) {
        data = new Object[maxRow][columnNames.length];
      }
    }

    public void randomize() {
      if (Utils.isDebugMode()) // Generate random claims in debug mode for testing
      {
        final int row = (int)(Math.random() * 100) + 25;
        final int col = columnNames.length;
        data = new Object[row][col];
        for (int i = 0; i < row; i++) {
          data[i][TIME_COL] = Utils.randomTime();
          data[i][USER_COL] = Utils.randomUser();
          data[i][NAME_COL] = Utils.randomName();
          data[i][COMP_COL] = Utils.randomComputer();
          data[i][COURSE_COL] = Utils.randomCourse();
          data[i][STATE_COL] = "Unresolved";
          data[i][CLAIM_COL] = Utils.randomProblem();
        }
      }
    }
  }

  public class ClaimCellRenderer extends JLabel implements TableCellRenderer {
    /**
     * 
     */
    private static final long serialVersionUID = -890613530716666519L;

    public ClaimCellRenderer() {
      super();
      setOpaque(true);
      setFont(font.deriveFont(0, 15));
      setToolTipText("");
    }

    @Override
    public Component getTableCellRendererComponent(final JTable table,
                                                   final Object color, final boolean isSelected, final boolean hasFocus,
                                                   final int row,
                                                   final int column) {
      if (table.getValueAt(row, column) == null) {
        setBackground(Color.WHITE);
        setForeground(Color.WHITE);
        return this;
      }

      setForeground(Color.BLACK);
      final String myText = (String)table.getValueAt(row, column);
      if (!myText.equals(getText())) { // greatly improves performance
        setText(myText);
        if (column == CLAIM_COL) setToolTipText(myText);
      }

      if (row == table.getSelectedRow()) setBackground(UIManager.getColor("Table.selectionBackground"));
      else setBackground(Color.WHITE);

      if (column == COURSE_COL) {
        for (int i = 0; i < Utils.classCodes.length; i++)
          if (Utils.classCodes[i].contains(myText)) {
            setBackground(Utils.classColors[i % Utils.classColors.length]);
            break;
          }
      }

      return this;
    }
  }

  public class ClaimSelectionListener implements ListSelectionListener {
    JTable table = null;

    ClaimSelectionListener(final JTable table) {
      this.table = table;
    }

    @Override
    public void valueChanged(final ListSelectionEvent e) {
      if (e.getValueIsAdjusting()) return;

      selectedRow = table.getSelectedRow();

      if (selectedRow < 0 || selectedRow > (claims.size()-1 - filterCount)) {
        button[BTN_SEND_INDEX].setEnabled(false);
        return;
      }

      button[BTN_SEND_INDEX].setEnabled(true);
      text.setText((String)table.getValueAt(selectedRow,
          ((ClaimsTableModel)table.getModel()).columnNames.length - 1));
    }
  }

  public class GUIButtonListener implements ActionListener {
    int _buttonId;

    public GUIButtonListener(final int buttonId) {
      _buttonId = buttonId;
    }

    @Override
    public void actionPerformed(final ActionEvent ev) {
      switch (_buttonId) {
        case BTN_SEND_INDEX: // send suggestion
          //sendHelp("Test", "0", text.getText());
          break;

        case BTN_FULLSCREEN_INDEX: // fullscreen problem
          fullscreenProblem();
          break;

        case BTN_COMMONLIST_INDEX: // quick suggestions
          break;

        default:
          Log.err("button id " + _buttonId + " does not exist");
      }
    }
  }

  // Listener for the menus at the top of the window
  public class GUIMenuListener implements ActionListener {
    ConsultantGUI gui;
    int           menu = -1, submenu = -1;

    public GUIMenuListener(final ConsultantGUI gui, final int menu, final int submenu) {
      this.gui = gui;
      this.menu = menu;
      this.submenu = submenu;
    }

    @Override
    public void actionPerformed(final ActionEvent ev) {
      switch (menu) {
        case FILE_INDEX: {
          switch (submenu) {
            case LOGIN_INDEX:
              consultant = Utils.promptForUsername();
              if (consultant != null) gui.consultant = consultant;
              break;
            case QUIT_INDEX:
              if (Utils.isLocalhostServer()) exit(0);

              while (true) {
                final JPasswordField pwd = new JPasswordField(10);
                final int response = JOptionPane.showConfirmDialog(null, pwd,
                    "Password:", JOptionPane.OK_CANCEL_OPTION);
                if (response < 0 || response == 2)
                  break;
                try {
                  final String test = new String(pwd.getPassword());
                  if (Utils.checkAdminPassword(test))
                    exit(0);
                } catch (final Exception e) {} finally {
                  Log.msg("That's not it...");
                }
              }

              break;
            default:
              break;
          }
          break;
        }

        case HELP_INDEX: {
          switch (submenu) {
            case ABOUT_INDEX:
              Utils.aboutMsg(gui, icon);
              break;
            default:
          }
        }
      }
    }
  }

  public class ProblemViewerFrame extends JFrame {
    /**
     * 
     */
    private static final long serialVersionUID = -6974533416989164733L;
    private final String[]    PVF_buttonText   = { "Send", "Cancel", "Common Problem List" };
    private final int         PVF_SEND_INDEX   = 0, PVF_CANCEL_INDEX = 1, PVF_COMMONLIST_INDEX = 2;
    private final JButton[]   PVF_buttons;
    private JTextArea         PVF_text         = null;

    public ProblemViewerFrame() {
      super();
      close(); // hide the frame initially
      setUndecorated(false);
      setResizable(true);

      final JPanel _panel = new JPanel(new BorderLayout());

      PVF_text = new JTextArea(text.getText(), 50, 60);
      PVF_text.setFont(font.deriveFont(0, 15));
      final JScrollPane _currentScrollPane = new JScrollPane(PVF_text);
      _currentScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
      _currentScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

      _panel.add(_currentScrollPane, BorderLayout.CENTER);

      final JPanel _buttonPanel = new JPanel(new GridLayout(PVF_buttonText.length, 1)); // +1 for checkbox panel
      PVF_buttons = new JButton[PVF_buttonText.length];
      for (int i = 0; i < PVF_buttonText.length; i++) {
        PVF_buttons[i] = new JButton("            " + PVF_buttonText[i]
            + "            ");
        PVF_buttons[i]
            .addActionListener(new CurrentClaimButtonListener(i));
        _buttonPanel.add(PVF_buttons[i]);
      }

      add(_panel, BorderLayout.CENTER);
      add(_buttonPanel, BorderLayout.EAST);

      PVF_buttons[PVF_SEND_INDEX].setEnabled(false);
    }

    public void open(final String firstname, final String course, final String waittime) {
      super.setTitle(firstname + " - " + course + " - " + waittime);
      super.setState(Frame.NORMAL);
      super.setExtendedState(Frame.MAXIMIZED_BOTH);
      super.setLocationRelativeTo(getRootPane());
      super.setAlwaysOnTop(true);
      super.setVisible(true);
      super.toFront();
    }

    public void close() {
      super.setVisible(false);
      super.setAlwaysOnTop(false);
      super.toBack();
    }

    public class CurrentClaimButtonListener implements ActionListener {
      int _buttonId;

      public CurrentClaimButtonListener(final int buttonId) {
        _buttonId = buttonId;
      }

      @Override
      public void actionPerformed(final ActionEvent ev) {
        switch (_buttonId) {
          case PVF_SEND_INDEX: // send suggestion
            //sendHelp("Temp", "0", PVF_text.getText());
            close();
            break;

          case PVF_CANCEL_INDEX: // close window
            close();
            break;

          case PVF_COMMONLIST_INDEX:
            break;

          default:
            Log.err("button id " + _buttonId + " does not exist");
        }
      }
    }
  }

  public static void main(final String[] args) {
    try {
      new ConsultantGUI();
    } catch (final Exception e) {
      System.err.println("fatal error: failed to initialize the consultant GUI");
      e.printStackTrace();
      System.exit(-1);
    }
  }
}
