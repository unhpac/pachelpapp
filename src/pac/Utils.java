/**
 * Defines global functions available to all other PAC classes.
 * You can safely assume that any function which returns a value handles its own exceptions,
 * returning either false or null if an error occurs.
 * 
 * @author Stephen Dunn
 */
package pac;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.Image;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class Utils {

  // general utility variables
  public static final Color[]            classColors       = { new Color(50, 205, 50),
    Color.BLUE, Color.RED, Color.ORANGE, new Color(230, 230, 250),
    new Color(169, 169, 169), Color.PINK };
  public static final String PACIP                         = "132.177.9";
  public static final String[]           classCodes        = { "cs405", "cs505", "cs506", "cs410",
    "cs415", "cs416", "cs515", "cs604", "cs900", "pac", "Unknown" };
  private static final String            adminPassword     = "eeef1c2f7d08d5f46a122458c2c445c5";
  public static final String             alpha             = "abcdefghijklmnopqrstuvwxyz";
  public static final String             timeFormat        = "HH:mm:ss";
  public static final String             dateFormat        = "MM/dd/yyyy";
  public static final String             timeAndDateFormat = Utils.dateFormat + " " + Utils.timeFormat;
  public final static String             bugLink           = "https://bitbucket.org/unhpac/pachelpapp/issues?status=new&status=open";
  public final static String             aboutText         = "<center><h3>PAC Help App v"
      + HelpApp.VERSION
      + " (Mar. 2013)</h3><br>"
      + "PAC Director: Matt Plumlee (<a href=\"mailto:mdp@cs.unh.edu\">mdp@cs.unh.edu</a>)<br>"
      + "Lead Programmer: Stephen Dunn (<a href=\"mailto:stephen@cs.unh.edu\">stephen@cs.unh.edu</a>)<br><br>"
      + "Please post bug reports, comments, or suggestions to:<br>"
      + "<a href=\"" + bugLink + "\">" + bugLink + "</a></center>";


  public final static String             aboutCaption      = "University of New Hampshire - Programming Assistance Center (PAC)";

  private static final DateTimeFormatter timeFormatter     = DateTimeFormat.forPattern(Utils.timeFormat);
  private static final DateTimeFormatter dateFormatter     = DateTimeFormat.forPattern(Utils.dateFormat);
  private static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(Utils.timeAndDateFormat);

  // config file-specific vars
  public static String                   defaultConfigPath = "./";
  public static String                   defaultConfig     = "config.properties";
  private static String                  currentConfig     = new String(Utils.defaultConfig);
  private static String                  currentConfigPath = new String(Utils.defaultConfigPath);
  private static Properties              configFile        = null;
  private static boolean                 configLoaded      = false;
  private final static String            configHeader      = " PAC Help App Configuration File for v"
      + HelpApp.VERSION
      + "\n# *****************************************************************************\n";
  private final static String            configFooter      = "\n# *****************************************************************************\n"
      + "# touchstamp:";

  public static enum CONFIG_VAR {
    DEBUG {
      @Override
      public String toString() {
        return "false";
      }
    },
    VERBOSE {
      @Override
      public String toString() {
        return "false";
      }
    },
    PORT {
      @Override
      public String toString() {
        return "10218";
      }
    },
    CLIENT_SERVER {
      @Override
      public String toString() {
        return "pac8.cs.unh.edu";
      }
    },
    LOCALHOST_SERVER {
      @Override
      public String toString() {
        return "false";
      }
    },
    REMOTE_CLIENT {
      @Override
      public String toString() {
        return "false";
      }
    },
    SERVER_LOG {
      @Override
      public String toString() {
        return "server.log";
      }
    },
    CLIENT_LOG {
      @Override
      public String toString() {
        return "client.log";
      }
    },
    GUI_LOG {
      @Override
      public String toString() {
        return "consultant.log";
      }
    },
    ATTACK {
      @Override
      public String toString() {
        return "000010";
      }
    },
    HEARTBEAT {
      @Override
      public String toString() {
        return "5";
      }
    },
    HEARTBEAT_DISCONNECT {
      @Override
      public String toString() {
        return "15";
      }
    },
    DATABASE {
      @Override
      public String toString() {
        return "claims.pac_db";
      }
    },
    CURRENT_CLAIM_ID {
      @Override
      public String toString() {
        return "0";
      }
    },
    UPD {
      @Override
      public String toString() {
        return defaultConfigPath;
      }
    };

    public String comments() {
      switch (this) {
        case DEBUG:
          return "toggle debug mode";
        case VERBOSE:
          return "toggle logging and console output verbosity";
        case PORT:
          return "port for host/client";
        case CLIENT_SERVER:
          return "server address for clients";
        case LOCALHOST_SERVER:
          return "only connect to 127.0.0.1 - for testing purposes";
        case REMOTE_CLIENT:
          return "is this client outside the PAC?";
        case SERVER_LOG:
          return "server log file";
        case CLIENT_LOG:
          return "client log file";
        case GUI_LOG:
          return "consultant GUI log file";
        case ATTACK:
          return "packets within this interval constitue an attack (\"hhmmss\")";
        case HEARTBEAT:
          return "gap between heartbeats (seconds), should be << HEARTBEAT_DISCONNECT";
        case HEARTBEAT_DISCONNECT:
          return "largest possible gap between heartbeats ";
        case DATABASE:
          return "server database file";
        case CURRENT_CLAIM_ID:
          return "current claim ID number";
        case UPD:
          return "user prefs directory for config/user settings";
      }
      return null;
    }
  }

//*****************************************************************************
// begin utility functions:
//*****************************************************************************
  public static boolean isConfigLoaded() {
    return Utils.configLoaded;
  }
  public static String currentConfigFile() {
    return Utils.currentConfig;
  }
  public static boolean isDebugMode() {
    return Utils.getConfigBoolValue(CONFIG_VAR.DEBUG);
  }
  public static boolean isVerboseMode() {
    return Utils.getConfigBoolValue(CONFIG_VAR.VERBOSE);
  }
  public static boolean isRemoteConnection() {
    return Utils.getConfigBoolValue(CONFIG_VAR.REMOTE_CLIENT);
  }
  public static boolean isLocalhostServer() {
    return Utils.getConfigBoolValue(CONFIG_VAR.LOCALHOST_SERVER);
  }
  public static String getGUILog() {
    return Utils.getConfigValue(CONFIG_VAR.GUI_LOG);
  }
  public static String getServerLog() {
    return Utils.getConfigValue(CONFIG_VAR.SERVER_LOG);
  }
  public static String getClientLog() {
    return Utils.getConfigValue(CONFIG_VAR.CLIENT_LOG);
  }
  public static String getClientServer() {
    return Utils.isLocalhostServer() ? "127.0.0.1" : Utils.getConfigValue(CONFIG_VAR.CLIENT_SERVER);
  }
  public static String getUserPrefsDir() {
    return Utils.getConfigValue(CONFIG_VAR.UPD);
  }
  public static Integer getHeartbeat() {
    return Utils.getConfigIntValue(CONFIG_VAR.HEARTBEAT);
  }
  public static Integer getHeartbeatDisconnect() {
    return Utils.getConfigIntValue(CONFIG_VAR.HEARTBEAT_DISCONNECT);
  }
  public static Integer getPort() {
    return Utils.getConfigIntValue(CONFIG_VAR.PORT);
  }
  public static Integer getDefaultPort() {
    try {
      return new Integer(CONFIG_VAR.PORT.toString());
    } catch (final Exception e) {
      return null;
    }
  }
  public static Integer getDefaultHeartbeat() {
    try {
      return new Integer(CONFIG_VAR.HEARTBEAT.toString());
    } catch (final Exception e) {
      return null;
    }
  }
  public static Integer getDefaultHeartbeatDisconnect() {
    try {
      return new Integer(CONFIG_VAR.HEARTBEAT_DISCONNECT.toString());
    } catch (final Exception e) {
      return null;
    }
  }

  public static String getConfigDefaultValue(final CONFIG_VAR val) {
    return val.toString();
  }

  public static Integer getConfigIntValue(final CONFIG_VAR var) {
    try {
      return new Integer(Utils.getConfigValue(var));
    } catch (final Exception e) {

      try {
        return new Integer(Utils.getConfigDefaultValue(var));
      } catch (Exception e2) {
        return null;
      }

    }
  }

  private static boolean getConfigBoolValue(final CONFIG_VAR var) {
    return Utils.getConfigValue(var).trim().toLowerCase().equals("true") ? true : false;
  }

  public static String getConfigValue(final CONFIG_VAR var) {
    synchronized (Utils.class) {
      if (Utils.configFile == null || !Utils.isConfigLoaded())
        return getConfigDefaultValue(var);
      return Utils.configFile.getProperty(var.name());
    }
  }

  public static boolean setConfigValue(final CONFIG_VAR var, final String value) {
    synchronized (Utils.class) {
      if (Utils.configFile == null || value == null || !Utils.isConfigLoaded()) return false;
      try {
        Utils.configFile.setProperty(var.name(), value);
      } catch (final Exception e) {
        return false;
      }
      return true;
    }
  }

  public static boolean loadConfig() {
    return Utils.loadConfig(Utils.defaultConfigPath, Utils.defaultConfig);
  }
  public static boolean loadConfig(final String path, final String config) {
    synchronized (Utils.class) {
      Utils.configLoaded = false;

      try {

        Utils.configFile = new Properties();
        Utils.configFile.load(new FileInputStream(path + config));
        Utils.currentConfigPath = path;
        Utils.configLoaded = true;


      } catch (final Exception e) {

        try {

          Utils.configFile = new Properties(); // reset from failure
          Utils.configFile.load(new FileInputStream(getUserPrefsDir() + config));
          Utils.currentConfigPath = getUserPrefsDir();
          Utils.configLoaded = true;

        } catch (Exception e2) {

          try {

            Utils.configFile = new Properties(); // reset from failure
            Utils.configFile.load(Utils.class.getResourceAsStream("/" + config));
            Utils.configLoaded = true;

          } catch (Exception e3) {

            Utils.configFile = null;
            return false;

          }
        }
      } finally {
        if (Utils.configLoaded == false) {
          Utils.configFile = null; return false;
        }
      }

      Utils.currentConfig = config;

      return true;
    }
  }

  public static boolean updateConfigFile() {
    return Utils.updateConfigFile(true);
  }

// FIX: createIfAbsent not being used as intended
  public static boolean updateConfigFile(final boolean createIfAbsent) {
    //if (!createIfAbsent && !configLoaded) return false;

    synchronized (Utils.class) {
      try {
        if (!Utils.configLoaded) {
          Utils.configFile = new Properties(); // no file was there, create one with defaults
          Utils.currentConfigPath = Utils.defaultConfigPath;
          Utils.currentConfig = Utils.defaultConfig;
        }

        OutputStream stream = null;

        try {
          stream = new FileOutputStream(Utils.currentConfigPath + Utils.currentConfig);
        } catch (Exception e) { }

        final CONFIG_VAR[] vars = CONFIG_VAR.values();

        String allComments = Utils.configHeader + "\n";
        for (CONFIG_VAR var : vars) {

          String useVal = var.toString(); // assume default
          try {
            final String curVal = Utils.getConfigValue(var); // override from memory
            if (curVal != null) useVal = curVal;
          } catch (final Exception e2) {} // using default

          Utils.configFile.setProperty(var.name(), useVal); // in case we just created a new properties file object
          allComments += "# " + var.comments() + "\n# " + var.name() + "=" + var.toString() + "\n";

        }
        allComments += Utils.configFooter;

        try {
          if (stream != null) { Utils.configFile.store(stream, allComments); stream.close(); }
        } catch (Exception e) { }

      } catch (final Exception e) {
        return false;
      }

      Utils.configLoaded = true;
      return true;
    }
  }

  public static void aboutMsg() {
    Utils.aboutMsg(null, null);
  }
  public static void aboutMsg(final JFrame gui, final ImageIcon icon) {
    JLabel label = new JLabel();
    Font font = label.getFont();

    StringBuffer style = new StringBuffer("font-family:" + font.getFamily() + ";");
    style.append("font-weight:" + (font.isBold() ? "bold" : "normal") + ";");
    style.append("font-size:" + font.getSize() + "pt;");

    JEditorPane ep = new JEditorPane("text/html", "<html><body style=\"" + style + "\">"
        + Utils.aboutText
        + "</body></html>");

    ep.addHyperlinkListener(new HyperlinkListener() {
      @Override
      public void hyperlinkUpdate(HyperlinkEvent e) {
        if (e.getEventType().equals(HyperlinkEvent.EventType.ACTIVATED))
          reportBug();
      }
    });
    ep.setEditable(false);
    ep.setBackground(label.getBackground());

    JOptionPane.showMessageDialog(gui, ep, Utils.aboutCaption,
        JOptionPane.INFORMATION_MESSAGE, icon);
  }

  public static void reportBug() {
    Desktop desktop = java.awt.Desktop.getDesktop();
    try {
      desktop.browse( new URI( bugLink ) );
    } catch (IOException e1) {
    } catch (URISyntaxException e1) {}
  }

  public static String stackTrace() {
    String errMsg = "";

    try {
      final StackTraceElement[] s = Thread.currentThread().getStackTrace();

      for (int i = 0; i < s.length; i++)
        if (i != 0 && i != 1) {
          errMsg += s[i].getClassName() + "::" + s[i].getMethodName()
              + ": line " + s[i].getLineNumber();
          if (i + 1 != s.length)
            errMsg += "\n";
        }
    } catch (final Exception e) {
      return null;
    }

    return errMsg;
  }

  public static String getOS() {
    String os = System.getProperty("os.name").toLowerCase();
    if (os.indexOf("mac") >= 0) return "mac";
    else if (os.indexOf("win") >= 0) return "win";
    else if (os.indexOf("nix") >= 0) return "nix";
    return os;
  }

  public static String getPWD() {
    URL location = HelpApp.class.getProtectionDomain().getCodeSource().getLocation();
    return location.getFile();
  }

  public static boolean checkIP(final String ip) {

    final String[] parts = ip.split("\\.");
    for (final String s : parts) {
      int i = -1;
      try {
        i = Integer.parseInt(s);
      } catch (final Exception e) {
        return false;
      }
      if (i < 0 || i > 255)
        return false;
    }

    if (ip.length() < 10)
      return false; // Too short to be valid



    return true;
  }

  public static boolean inPAC(final String ip) {
    if (ip == null || ip.length() < 9) return false;
    if (!ip.substring(0, 9).equals(PACIP)
        && !ip.substring(0, 9).equals("127.0.0.1"))
      return true;
    return false;
  }

  public static boolean checkPort(final int port) {
    if (port < 256 || port > 65536) return false;
    return true;
  }

  public static String getTime() {
    return Utils.timeFormatter.print(new DateTime());
  }

  public static String getDate() {
    return Utils.dateFormatter.print(new DateTime());
  }

  public static String getTimeAndDate() {
    return Utils.dateTimeFormatter.print(new DateTime());
  }

  public static String getUsername() {
    final ArrayList<String> temp = Utils.shell("whoami");
    if (temp == null || temp.size() < 1 || temp.get(0).length() < 1) return null;
    return temp.get(0);
  }

  public static String getFirstName() {
    return Utils.getFirstName(Utils.getUsername());
  }

  /*
   * Sample return from shell on "ypcat passwd | grep USERNAME" request:
   * snk8:*:25956:445:Stephen N Dunn(PAC,4/2010):/home/csu/snk8:/bin/csh
   */
  public static String getFirstName(final String username) {
    try {
      final ArrayList<String> temp = Utils.shell("./name");
      if (temp == null || temp.size() < 1) return null;
      final String parts[] = temp.get(0).split(":");
      if (parts == null || parts.length < 5) return null;
      final String subparts[] = parts[4].split(" ");
      if (subparts == null || subparts.length < 1) return null;
      return subparts[0];
    } catch (final Exception e) {}

    return null;
  }

  public static ArrayList<String> shell(final String cmd) {
    ArrayList<String> ret;
    try {
      ret = new ArrayList<String>();
      final Process p = Runtime.getRuntime().exec(cmd);
      final Scanner sc = new Scanner(p.getInputStream());
      while (sc.hasNext())
        ret.add(sc.nextLine());
    } catch (final Exception e) {
      return null;
    }

    return ret;
  }

  public static String getGroups() { return getGroups(""); }
  public static String getGroups(String name) {
    String groups = "";

    try {
      if (name.equals("pacusr")) return Utils.classCodes[0];

      final ArrayList<String> temp = Utils.shell( ("groups " + name).trim() );
      if (temp.size() <= 0)
        return null;
      final String allGroups = temp.get(0);
      if (allGroups.contains("No such user")) return null;

      for (final String someClass : Utils.classCodes)
        if (allGroups.contains(someClass)) groups += someClass + " ";

    } catch (final Exception e) {
      return null;
    }

    if (groups == null || groups.length() <= 0) return Claim.UNKNOWN_VALUE;
    return groups.trim(); // quotes prevent potential errors for large group members
  }

  public static String getHostname() {
    try {
      final ArrayList<String> temp = Utils.shell("hostname");
      if (temp.size() <= 0) return null;
      return temp.get(0).trim();
    } catch (final Exception e) {}
    return Claim.UNKNOWN_VALUE;
  }

  public static String promptForUsername() {
    String temp = "";
    boolean invalid = true;
    while (invalid)
      try {
        temp = JOptionPane.showInputDialog(null, "Please enter a username:", "Hi!", 1).trim();
        if (temp != null && !Utils.isRestrictedUsername(temp)) invalid = false;
        else Log.msg("The username you entered is too short or is restricted. Please try another.");
      } catch (final Exception e) {
        return null;
      }
    if (temp.equals("")) return null;
    return temp;
  }

  public static boolean isRestrictedUsername(final String username) {
    return false;/*
    if (username == null) return false;
    final String temp = username.toLowerCase(); // Move the restricted names to a file and load them
    if (temp.equals("admin") || temp.equals("snk8") || temp.equals("administrator")) return true;
    return false;*/
  }

  public static String encryptString(final String enc) {
    try {
      final byte[] bytes = enc.getBytes("UTF-8");
      final MessageDigest md = MessageDigest.getInstance("MD5");
      final BigInteger bigInt = new BigInteger(1, md.digest(bytes));
      return bigInt.toString(16);
    } catch (final Exception e) {}
    return null;
  }

  public static boolean checkAdminPassword(final String check) {
    try {
      final String enc = Utils.encryptString(check);
      if (enc.equals(Utils.adminPassword)) return true;
    } catch (final Exception e) {}
    return false;
  }

  public static String fixTimeFormatting(final String time) {
    final String[] parts = time.split(":");
    String padded = "";

    for (final String s : parts) {
      if (padded.length() > 0) padded += ":";
      if (s.length() < 2) padded += "0" + s;
      else padded += s;
    }

    return padded;
  }

  public static String timeElapsed(final String timeStarted, final String timeFinished) {
    String ret = "";
    if (timeStarted == null || timeFinished == null
        || timeStarted.equals("") || timeFinished.equals(""))
      return null;

    try {

      final LocalTime fTime = Utils.timeFormatter.parseLocalTime(timeFinished);
      final LocalTime sTime = Utils.timeFormatter.parseLocalTime(timeStarted);
      final Period diff = Period.fieldDifference(sTime, fTime);
      int dH = diff.getHours(), dM = diff.getMinutes(), dS = diff.getSeconds();
      if (dH < 0) dH += 24;
      if (dM < 0) dM += 60;
      if (dS < 0) dS += 60;
      ret = dH + ":" + dM + ":" + dS;

    } catch (final Exception e) {
      return null;
    }

    return ret;
  }

  public static Image getImage(String path) {
    Image image = null;
    try {

      image = ImageIO.read(new File(path));

    } catch (final Exception e) {

      try {
        image = ImageIO.read(Utils.class.getResourceAsStream("/" + path));
      } catch (Exception e2) {
        return null;
      }

    }
    return image;
  }

  public static void sleep(final long millis) {
    try {
      Thread.sleep(millis);
    } catch (final Exception e) {}
  }

  public static String randomUser() {
    return "" + Utils.alpha.charAt((int)(Math.random() * 26))
        + Utils.alpha.charAt((int)(Math.random() * 26))
        + Utils.alpha.charAt((int)(Math.random() * 26))
        + (int)(Math.random() * 9) + "" + (int)(Math.random() * 9);
  }

  public static String randomComputer() {
    return "PAC" + (int)(Math.random() * 9) + ""
        + (int)(Math.random() * 9);
  }

  public static String randomTime() {
    return (int)(Math.random() * 9) + "" + (int)(Math.random() * 9) + ":"
        + (int)(Math.random() * 9) + "" + (int)(Math.random() * 9)
        + ":" + (int)(Math.random() * 9) + ""
        + (int)(Math.random() * 9);
  }

  public static String randomProblem() {
    final String[] help = {
        "Whoever wrote this program needs a raise",
        "Edit: $$$ for code",
        "Programmers are sexy",
        "They call me the exterminator, cause I debug",
        "My ex-gf took lessons with the devil. Wonder how much she charged him.",
        "Sometimes I get on TV and I wanna let loose, but I can't and it's cool for Tom Green",
        "Don't stop Steve now, he's having such a good time",
        "Notorious!",
        "Can you call the next version 2PAC?",
        "I'm running Linux man, I thought you GNU?",
        "I can't program and I hate this",
        "So how do I see plus plus?",
        "This program is amazing",
        "Don't mean to \"bug\" you, but...",
        "Is it too late to switch to IT?",
    "I can't retake this class a 3rd time." };
    final int choice = (int)(Math.random() * (help.length - 1));
    return help[choice];
  }

  public static String randomName() {
    final String[] names = { "John", "Paul", "George", "Ringo", "Neitzsche",
        "Dochevsky", "Napolean", "Obama", "Plumlee", "Ludacris",
        "Steve Jobs", "von Neuman", "Turing", "Prof. X", "Palpatine",
        "Keyser Soze", "Hagrid", "Bochert", "Hatcher", "Charpov", "Yost", "Wheeler" };
    final int choice = (int)(Math.random() * (names.length - 1));
    return names[choice];
  }

  public static String randomCourse() {
    final String[] myClassCodes = { "cs405", "cs410", "cs415", "cs416", "cs515",
    "cs604" };
    final int choice = (int)(Math.random() * (myClassCodes.length - 1));
    return myClassCodes[choice];
  }

}
