/**
 * Manages the PAC Log and contains global generic functions for the rest of the PAC Code.
 * 
 * @author Stephen Dunn
 */
package pac;

import java.io.BufferedWriter;
import java.io.FileWriter;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public final class Log {
  private static String         log    = "debug.log";
  private static BufferedWriter writer = null;
  private static boolean        debug  = false, verbose = false, init = false; // grabbed on initialization of log

  private Log() {} // singleton model

  public static void msg(final String message) {
    Log.msg(null, message, null, JOptionPane.INFORMATION_MESSAGE);
  }

  public static void msg(final JFrame frame, final String message) {
    Log.msg(frame, message, null, JOptionPane.INFORMATION_MESSAGE);
  }

  public static void msg(final String message, final String caption) {
    Log.msg(null, message, caption, JOptionPane.INFORMATION_MESSAGE);
  }

  public static void msg(final JFrame frame, final String message, final String caption, final int icon) {
    if (Log.verbose) Log.writeToLog(message);
    JOptionPane.showMessageDialog(frame, message, caption, icon);
  }

  public static String getLogFilename() {
    return Log.log;
  }

  public static void out(final String text) {
    Log.printf("%s\n", text);
  }

  public static void err(final String text) {
    Log.err(text, false);
  }

  public static void err(String text, final boolean addStackTrace) {
    text = Utils.getTime() + ": *****error: " + text + "\n";
    if (addStackTrace) text += Utils.stackTrace() + "\n";
    Log.printf(true, text);
  }

  public static void printf(final String format, final Object... args) {
    Log.printf(false, format, args);
  }

  public static void printf(final boolean err, final String format, final Object... args) {
    final String text = new String(Utils.getTime() + ": " + format);
    synchronized (Log.class) {
      if (err) try {
        System.err.printf(text, args);
      } catch (final Exception e) {
        Log.err("printf formatting error");
      }
      else try {
        System.out.printf(text, args);
      } catch (final Exception e) {
        Log.err("printf formatting error");
      }
      if (Log.verbose) Log.writeToLog(String.format(text, args));
    }
  }

  public static void debugLog(final String text) {
    if (Log.debug) Log.writeToLog(text);
  }
  public static void writeToLog(final String text)
  {
    synchronized (Log.class) {
      if (!Log.init) return;
      try {
        if (Log.writer != null) {
          Log.writer.write(text /* + System.getProperty("line.separator")*/);
          Log.writer.flush();
        }
      } catch (final Exception e) {
        System.err.println("failed to log an error because the writer is null");
      }
    }
  }

  public static boolean createLog(final String logFile) {
    try {
      Log.debug = Utils.isDebugMode();
      Log.verbose = Utils.isVerboseMode();
    } catch (final Exception e) {}

    synchronized (Log.class) {
      try {
        Log.writer = new BufferedWriter(new FileWriter(Utils.getUserPrefsDir() + logFile, true));
        Log.writer.write("PAC log file \"" + logFile + "\" initialized ("
            + Utils.getTimeAndDate() + ")"
            + System.getProperty("line.separator"));
        Log.writer.flush();
      } catch (final Exception e) {
        System.err.println(Utils.getTimeAndDate() +
            ": could not open or create the log file \"" +
            Utils.getUserPrefsDir() + logFile + "\"");
        Log.writer = null;
        return false;
      }
      Log.init = true;
    }

    return true;
  }

  public static void closeLog() {
    synchronized (Log.class) {
      try {
        if (!Log.init) return;
        if (Log.writer != null)
          Log.writer.close(); // Close will flush first
        Log.writer = null;
      } catch (final Exception e) {
        System.err.println("something went wrong while closing the log file");
      }
      Log.init = false;
    }
  }
}
