/**
 * Class to organize and manage a claims database.
 * 
 * @author Stephen Dunn
 */

package pac;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;

import pac.Utils.CONFIG_VAR;

public class ClaimManager {
  public static final String claimSeparatorSymbol     = "\003";
  public static final String claimPartSeparatorSymbol = "\004";
  public static String       claimDatabase            = "claims.pac_db"; // default database
  public static enum CLAIM_STATE {
    HELPING, CANCELED, PROGRAM_FEEDBACK, CONSULTANT_FEEDBACK, RESOLVED, UNRESOLVED, ERROR
  }

  private ArrayList<Claim> claims;
  private int              curId = 0; // for maintaining claim ids

  public ClaimManager() throws NullPointerException {

    if (!Utils.isConfigLoaded()) Log.err("failed to load the current claim id from the config file, defaulting to " + curId);
    else try {
      curId = Integer.parseInt(Utils.getConfigValue(CONFIG_VAR.CURRENT_CLAIM_ID));
    } catch (final Exception e) {
      try {
        final String s = Utils.getConfigValue(CONFIG_VAR.CURRENT_CLAIM_ID);
        if (s != null) Log.err("invalid claim id number \"" + s + "\" specified in config file \""
            + Utils.currentConfigFile() + "\"");
        else {
          Log.err("fatal configuration error");
          throw new NullPointerException();
        }
      } catch (final Exception e2) {
        Log.err("fatal internal error");
        throw new NullPointerException();
      }
    }

    try {
      claims = new ArrayList<Claim>();
    } catch (final Exception e) {
      Log.err("fatal error");
      throw new NullPointerException();
    }
  }

  // Cancel a particular claim
  public synchronized void editClaimString(final int id, final String claim) {
    for (final Claim cur : claims)
      if (cur.id() == id) {
        cur.claim = claim;
        return;
      }
  }

  public synchronized void editClaimState(final int id, final CLAIM_STATE state) {
    for (final Claim cur : claims)
      if (cur.id() == id) {
        cur.setState(state);
        return;
      }
  }

  public synchronized void attachConsultant(final int id, final String consultant) {
    for (final Claim cur : claims)
      if (cur.id() == id) {
        cur.consultant = consultant;
        return;
      }
  }

  public synchronized void attachSuggestion(final String consultant, final int id,
                                            final String suggestion) {
    final Claim temp = getClaimById(id);
    if (temp == null) {
      Log.err("invalid claim id for suggestion attachment: "
          + id + "\n" + suggestion);
      return;
    }
    temp.attachSuggestion(consultant, suggestion, Utils.getTimeAndDate());
  }

  public synchronized Claim getClaimById(final int id) {
    if (claims == null) return null;
    for (final Claim cur : claims)
      if (cur.id() == id) return cur;
    return null;
  }

  public synchronized ArrayList<Claim> getClaimsByUsername(final String username) {
    final ArrayList<Claim> ret = new ArrayList<Claim>();
    for (final Claim cur : claims)
      if (cur.username.equals(username)) ret.add(cur);
    if (ret.size() <= 0) return null;
    return ret;
  }

  public synchronized String getClassByUsername(final String username) {
    for (final Claim cur : claims)
      if (cur.username.equals(username)) return cur.groups;
    return null;
  }

  public synchronized String getClaimStringByUsername(final String username) {
    for (final Claim cur : claims)
      if (cur.username.equals(username)) return cur.claim;
    return null;
  }

  public synchronized void cancel(final int id) {
    editClaimState(id, CLAIM_STATE.CANCELED);
  }

  public synchronized void resolve(final int id, final String consultant) {
    final Claim temp = getClaimById(id);
    if (temp != null)
      temp.finish(consultant);
  }

  public synchronized void clear() {
    claims.clear();
  }

  public int size() {
    return claims.size();
  }

  public Claim remove(final int i) {
    try {
      return claims.remove(i);
    } catch (final Exception e) {
      Log.err("no claim at index " + i);
    }
    return null;
  }

  public void update(Claim c) {
    Claim temp = getClaimById(c.id());
    if (temp == null) add(c);
    else {
      temp.claim = c.claim;
      temp.computer = c.computer;
      temp.consultant = c.consultant;
      temp.dateFinished = c.dateFinished;
      temp.dateStarted = c.dateStarted;
      temp.firstName = c.firstName;
      temp.groups = c.groups;
      temp.timeFinished = c.timeFinished;
      temp.timeStarted = c.timeStarted;
      temp.username = c.username;
      temp.suggestions = c.suggestions;
      temp.state = c.state;
    }
  }

  public String getClaimString(final int i) {
    if (i > size() - 1 || i < 0) return null;
    return claims.get(i).claim;
  }

  public String getClaimUsername(final int i) {
    if (i > size() - 1 || i < 0) return null;
    return claims.get(i).username;
  }

  public String getClaimConsultant(final int i) {
    if (i > size() - 1 || i < 0) return null;
    return claims.get(i).consultant;
  }

  public int getClaimId(final int i) {
    if (i > size() - 1 || i < 0) return -1;
    return claims.get(i).id();
  }

  public Claim get(final int i) {
    if (i > size() - 1 || i < 0) return null;
    return claims.get(i);
  }

  public ArrayList<Claim> getClaims() {
    return claims;
  }

  public synchronized String[] getClaimStrings() {
    return getClaimStrings(false);
  }

  public synchronized String[] getClaimStrings(final boolean ignoreClosedClaims) {
    final String tempFull = getFullClaimsAsString(ignoreClosedClaims);
    if (tempFull == null)
      return null;

    return tempFull.split(ClaimManager.claimSeparatorSymbol);
  }

  public synchronized String getFullClaimsAsString() {
    return getFullClaimsAsString(true);
  }

  public synchronized String getFullClaimsAsString(final boolean ignoreClosedClaims) {
    String allClaims = "";

    for (final Claim c : claims)
      try {
        allClaims = allClaims + c.toString() + ClaimManager.claimSeparatorSymbol;
      } catch (final Exception e) {
        // claim couldn't be saved, probably a bad id
      }

    return allClaims;
  }

  public synchronized int add(final String claim, final String firstName,
                              final String username, final String groups, final String computer,
                              final String creationDateAndTime, final String finishDateAndTime,
                              final String claimState, final String suggestions) {

    final Claim c = new Claim.ClaimBuilder().id(curId).claim(claim)
        .firstName(firstName).username(username).groups(groups)
        .computer(computer).creationDateAndTime(creationDateAndTime)
        .finishDateAndTime(finishDateAndTime).suggestions(suggestions)
        .build();

    claims.add(c);
    curId++;

    return curId - 1;
  }

  public synchronized void add(final Claim claim) {
    claims.add(claim);
  }

  public synchronized boolean save() {
    return save(ClaimManager.claimDatabase);
  }
  public synchronized boolean save(final String fname) {
    PrintWriter out = null;
    try {
      out = new PrintWriter(new BufferedWriter(new FileWriter(fname)));
    } catch (final Exception e) {
      Log.err("failed to open the file: " + fname);
      return false;
    }
    String s = null;
    try {
      s = getFullClaimsAsString();
    } catch (final Exception e) {
      s = null;
    } finally {
      if (s == null) {
        Log.out("No claims to save.");
        out.close();
        return true;
      }
    }

    try {
      if (s != null)
        out.write(s);
      out.close();
    } catch (final Exception e) {
      Log.err("failed to save database");
      return false;
    }
    return true;
  }

  public boolean load() {
    return load(ClaimManager.claimDatabase);
  }

  public boolean load(final String fname) {
    String data = "";
    try {
      final BufferedReader in = new BufferedReader(new FileReader(fname));
      String temp = "";
      while ((temp = in.readLine()) != null)
        data += temp;
      in.close();
    } catch (final Exception e) {
      return false;
    }
    loadFromString(data);
    return true;
  }

  public synchronized void loadFromString(final String claimStr)
  {
    if (claimStr == null || claimStr.length() <= 0) {
      Log.out("There is no data to load.");
      return;
    }

    claims = new ArrayList<Claim>();
    final String parts[] = claimStr.split(ClaimManager.claimSeparatorSymbol);
    for (int i = 0; i < parts.length; i++)
      try {
        final Claim temp = new Claim(parts[i]);
        claims.add(temp);
        curId = temp.id() + 1;
      } catch (final Exception e) {
        Log.err("bad claim received: \"" + parts[i] + "\"\nload fail at claim index: " + i);
      }
  }

  public static SuggestionManager newSuggestionManager() {
    return new SuggestionManager();
  }

  public Claim randomClaim(final int myId) {

    return new Claim.ClaimBuilder().id(myId).claim(Utils.randomProblem())
        .firstName(Utils.randomName()).username(Utils.randomUser())
        .computer(Utils.randomComputer())
        .creationDateAndTime(Utils.getDate() + " " + Utils.randomTime())
        .claimState(CLAIM_STATE.UNRESOLVED).groups(Utils.randomCourse()).build();

  }

  public void randomClaims() {
    randomClaims(25, 100, 0);
  }

  public void randomClaims(final int min, final int max, final int startId) {
    claims = new ArrayList<Claim>();
    curId = startId;
    for (int i = 0; i < (int)(Math.random() * max) + min; i++) {
      add(randomClaim(curId));
      curId++;
    }
  }

}
