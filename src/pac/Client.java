/**
 * Creates a client object for connecting to the server.
 * 
 * @author Stephen Dunn
 */
package pac;

import java.net.InetAddress;
import java.util.Timer;
import java.util.TimerTask;

import pac.Packet.PACKET;
import pac.Utils.CONFIG_VAR;

public class Client
{
  private final Integer  lock = new Integer(0);
  private Packet pending = null;
  private Socket         socket;
  private Timer          timer;
  private Integer        port;
  private String         serverString = "127.0.0.1";
  private static Integer heartbeat;
  private boolean        connected = false;

  /********************************************************************
   * Client Constructors
   ******************************************************************/
  public Client() throws NullPointerException {
    this( Utils.getPort() );
  }

  public Client(final int port) throws NullPointerException {

    // get port
    if (!Utils.checkPort(port)) {
      try {
        this.port = Utils.getPort();
      } catch (final Exception e) {}
      if (this.port == null || !Utils.checkPort(this.port)) try {
        this.port = Integer.parseInt(Utils.getConfigDefaultValue(CONFIG_VAR.PORT));
      } catch (final Exception e2) {
        throw new NullPointerException();
      }
    } else {
      this.port = port;
    }

    // get heartbeat
    try {
      Client.heartbeat = Utils.getHeartbeat();
    } catch (final Exception e) {} finally {
      if (Client.heartbeat == null || Client.heartbeat == 0) try {
        Client.heartbeat = Utils.getDefaultHeartbeat();
      } catch (final Exception e2) {
        throw new NullPointerException();
      }
    }

    try {
      serverString = Utils.getClientServer();
    } catch (final Exception e) {
      try {
        serverString = Utils.getConfigDefaultValue(Utils.CONFIG_VAR.CLIENT_SERVER);
      } catch (final Exception e2) {
        throw new NullPointerException();
      }
    }

  }

  public Client(final int port, final Socket socket) throws NullPointerException {
    this(port);
    if (socket == null) throw new NullPointerException();
    this.socket = socket;
  }

  /********************************************************************
   * Basic Client Functions
   ******************************************************************/

  public synchronized void setPort(final int port) {
    this.port = new Integer(port);
  }

  public int getPort() {
    return port.intValue();
  }

  public synchronized void setServer(final String server) {
    this.serverString = server;
  }

  public String getServer() {
    return serverString;
  }

  public InetAddress getIP() {
    return socket.getClientSocket().getLocalAddress();
  }

  public String getIPString() {
    return getIP().getHostAddress();
  }

  public Packet getPendingData() {
    synchronized(lock) { return pending; }
  }

  public synchronized int connect()
  {
    return connect(serverString, port, Utils.getUsername(),
        Utils.getHostname(), Utils.getFirstName());
  }

  public synchronized int connect(final String username, final String computer, final String firstname) {
    return connect(serverString, port, username, computer, firstname);
  }

  public synchronized int connect(final String server, final int port, final String username,
                                  final String computer, final String firstname)
  {
    disconnect(true); // silent disconnect

    if (username == null) {
      Log.err("a null username was passed");
      return -1;
    } else if (server == null) {
      Log.err("a null server address was passed");
      return -1;
    } else if (firstname == null) {
      Log.err("a null fistname was passed");
      return -1;
    } else if (computer == null) {
      Log.err("a null computer name was passed");
      return -1;
    }

    try {
      socket = new Socket(server, port, false);
    } catch (final Exception e) {
      return -2;
    }

    if (!socket.getInitState()) return -3;

    try {
      sendConnect(username, computer, firstname);
    } catch (final Exception e) {
      return -4;
    }

    startHeartbeat();
    connected = true;

    return 0;
  }

  public synchronized void startHeartbeat() {
    if (timer != null) timer.cancel();
    timer = new Timer();
    timer.schedule(new ClientHeartbeat(this), 0, Client.heartbeat * 1000);
  }

  public synchronized void disconnect() { disconnect(true); }
  public synchronized void disconnect(boolean silent)
  {
    connected = false;

    if (timer != null) timer.cancel();
    timer = null;
    connected = false;

    if (socket != null) {
      try {
        socket.send(new Packet(PACKET.DISCONNECT));
      } catch (final Exception e) {} // might not be a real error if server died first
      try {
        socket.close();
      } catch (final Exception e) {}

      socket = null;
      if (!silent)
        Log.out("disconnected from the server");
    }
  }

  /**
   * Sends a heartbeat message to check for active connection.
   * 
   * @return true if alive, false if dead
   */
  public synchronized boolean isConnected() {
    return connected;
  }

  public synchronized boolean ping() {
    if (socket == null) return false;
    try {
      if (sendHeartbeat() != 0) {
        disconnect();
        return false;
      }
      return true;
    } catch (final Exception e) {}
    disconnect();
    return false;
  }

  public synchronized Packet receive() {
    String temp = null;

    synchronized (lock) {

      if (socket == null) {
        disconnect();
        Log.err("cannot receive data because client is not connected");
        return null;
      }

      try {
        temp = socket.receive();
        if (temp == null) {
          disconnect();
          Log.err("null packet received from socket");
          return null;
        }
      } catch (final Exception e) {
        disconnect();
        Log.err("an error occured while receiving data");
        return null;
      }

      return new Packet(temp);
    }
  }

  public synchronized int send(final Packet s) {
    return send(s.toString());
  }

  public synchronized int send(final Packet s, final boolean ignoreAttack) {
    return send(s.toString(), ignoreAttack);
  }

  // Use ignoreAttack = true for special client/server communications that should ignore the DOS prevention.
  public synchronized int send(final String s, final boolean ignoreAttack)
  {
    synchronized (lock) {
      try {
        if (socket == null) {
          disconnect();
          return -1;
        }
        if (socket.send(s) != 0) {
          disconnect();
          return -2;
        }
      } catch (final Exception e) {
        disconnect();
        return -3;
      }
    }
    return 0;
  }

  public synchronized int send(final String s) {
    return send(s, false);
  }

  public synchronized int sendClaim(final String s) {
    return send(new Packet(PACKET.CLAIM, s));
  }

  public synchronized int sendHelpingClaim(final String s) {
    return send(new Packet(PACKET.HELPING, s));
  }

  public synchronized int sendEditClaim(final String s) {
    return send(new Packet(PACKET.EDIT, s));
  }

  public synchronized int sendResolvedClaim(final String s) {
    return send(new Packet(PACKET.RESOLVED, s));
  }
  public synchronized int sendUnresolvedClaim() {
    return send(new Packet(PACKET.UNRESOLVED));
  }

  public synchronized int sendCancelClaim() {
    return send(new Packet(PACKET.CANCEL));
  }

  public synchronized int sendGetClaims() {
    return send(new Packet(PACKET.GET_CLAIMS));
  }

  public synchronized int sendHeartbeat() {
    return send(new Packet(PACKET.HEARTBEAT));
  }

  private synchronized int sendConnect(String user, String comp, String name) {
    final Packet p = new Packet(PACKET.CONNECT,
        user + ClaimManager.claimPartSeparatorSymbol +
        comp + ClaimManager.claimPartSeparatorSymbol +
        name);
    return send(p);
  }

  public synchronized int sendSuggestion(final int id, final String cons, final String usr, final String msg) {
    final Packet p = new Packet(PACKET.SUGGESTION,
        id + ClaimManager.claimPartSeparatorSymbol +
        cons + ClaimManager.claimPartSeparatorSymbol +
        usr + ClaimManager.claimPartSeparatorSymbol +
        msg);
    return send(p);
  }

  public synchronized String getServerDateAndTime() {
    Packet DaT;

    try {
      final int ret = send(new Packet(PACKET.DATE_TIME));
      if (ret != 0) {
        Log.err("failed to get the date and time from the server");
        return null;
      }
      DaT = receive();
    } catch (final Exception e) {
      Log.err("failed to get the date and time from the server");
      return null;
    }

    return DaT.getData();
  }

  /**
   * Client Heartbeat
   */
  class ClientHeartbeat extends TimerTask {
    Client client;

    ClientHeartbeat(final Client client) {
      this.client = client;
    }

    @Override
    public void run() {
      if (client == null) {
        cancel();
        return;
      }
      else if (client.sendHeartbeat() != 0) { // Try to send...
        try {
          client.disconnect();
          client = null;
        } catch (final Exception e) {}
        cancel();
      }

    }
  }

}
