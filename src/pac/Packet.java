/**
 * Creates a packet for sending through the PACClient/PACServer system.
 * 
 * @author Stephen Dunn
 */

package pac;

public class Packet {
  public PACKET               type;
  public String               data;
  private static final String packetSeparatorSymbol  = "@@";
  private static final String newlineSeparatorSymbol = "\002";

  public static enum PACKET {
    CLAIM, RESOLVED, HELPING, UNRESOLVED, CANCEL, EDIT, CONNECT, HEARTBEAT, DISCONNECT,
    GET_CLAIMS, DATE_TIME, CONSULTANT_LOGIN, SUGGESTION, INVALID
  }

  public Packet(String s) // For receiving a packet
  {
    if (s == null) {
      Log.err("a null string was received");
      return;
    }

    /********* interpret the packet: ***************/
    try {
      s = s.replace(Packet.newlineSeparatorSymbol, "\n"); // Put the newlines back in that were
      // removed for sending
      final String parts[] = s.split(Packet.packetSeparatorSymbol); // Get the type out of the packet
      type = Enum.valueOf(PACKET.class, parts[0]);
      if (parts.length >= 2) data = parts[1]; // Extract the message
    } catch (final Exception e) {
      Log.err("invalid string received: " + s);
    }
  }

  public Packet(final PACKET type) { // used for sending a packet
    this(type, null);
  }

  public Packet(final PACKET type, final String s) // used for sending a packet
  {
    this.type = type;
    data = "";

    if (s != null) {
      data = new String(s);
      if (type == PACKET.CLAIM || type == PACKET.HELPING) // remove newlines
        data = data.replace("\n", Packet.newlineSeparatorSymbol);
    }
  }

  public PACKET getType() {
    return type;
  }

  public String getData() {
    return data;
  }

  @Override
  public String toString() {
    return type.name() + Packet.packetSeparatorSymbol + data;
  }

  public boolean is(final PACKET type) {
    return type == this.type ? true : false;
  }

  @Override
  public boolean equals(Object type) {
    return is((PACKET)type);
  }
}
