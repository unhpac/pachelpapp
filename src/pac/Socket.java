/**
 * Manages a socket for client/server communication.
 * 
 * @author Stephen Dunn
 */

package pac;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.UnknownHostException;

public class Socket
{
  /********************************************************************
   * Member Variables
   ******************************************************************/
  final int                     defaultPort = 10218;
  private int                   myPort      = 10218;
  private final int             lastPort    = myPort;

  private String                myServer    = "127.0.0.1";

  private java.net.Socket       client      = null;
  private java.net.ServerSocket server      = null;

  private PrintWriter           write       = null;
  private BufferedReader        read        = null;

  private boolean               init        = false, serving = false;

  /********************************************************************
   * Constructors
   ******************************************************************/
  public Socket(final int port) throws Exception // Basic Server Constructor
  {
    this("127.0.0.1", port, true);
  }

  public Socket(final String server, final int port, final boolean serving) throws Exception
  {
    myPort = port;
    myServer = server;
    this.serving = serving; // Save critical values

    try
    {
      if (!serving)
      {
        client = new java.net.Socket(server, port);
        write = new PrintWriter(client.getOutputStream(), true);
        read = new BufferedReader(new InputStreamReader(client.getInputStream()));
      } else this.server = new ServerSocket(myPort);
    } catch (final UnknownHostException e)
    {
      Log.err("the host \"" + server + "\" is unreachable");
      init = false;
      return;
    } catch (final Exception e)
    {
      if (serving)
        Log.err("failure to listen on port " + myPort);
      read = null;
      write = null;
      init = false;
      throw new Exception();
    }

    init = true;
  }

  // this is needed for accepting incoming connections
  public Socket(final java.net.Socket sock) throws Exception
  {
    client = sock;

    if (sock == null)
      Log.err("bad socket received");

    try {

      write = new PrintWriter(sock.getOutputStream(), true);
      read = new BufferedReader(new InputStreamReader(sock.getInputStream()));

    } catch (final UnknownHostException e)
    {
      Log.err("the host is unreachable");
      init = false;
      throw new Exception();
    } catch (final Exception e)
    {
      Log.err("could not create I/O objects");
      init = false;
      throw new Exception();
    }

    init = true;
  }

  /********************************************************************
   * Basic Socket Functions
   ******************************************************************/

  public boolean getInitState()
  {
    return init;
  }

  public java.net.Socket getClientSocket()
  {
    return client;
  }

  public ServerSocket getServerSocket()
  {
    return server;
  }

  public void close()
  {
    try
    {
      if (write != null)
        write.close();
      if (read != null)
        read.close();
    } catch (final Exception e) {}

    read = null;
    write = null;
    init = false;

    // If I'm the server......
    if (serving) // If we're the server socket, disconnect everybody.
    {
      try {
        if (server != null)
          server.close(); // We don't need to reset read/write because they are not used in servers.
        server = null;
      } catch (final Exception e)
      {
        Log.err("failed to close a socket");
        return;
      }

      serving = false;
      init = false;
      return;
    }

    // if I'm the client....
    try {
      if (client != null)
      {
        client.close(); // Kill the client.
        client = null;
      }
    } catch (final Exception e)
    {
      Log.err("failed to close a socket");
      return;
    }
  }

  public Socket accept() throws Exception
  {
    if (!init)
      return null;

    Socket temp = null;

    if (server == null)
    {
      Log.err("bad socket: cannot accept new connections");
      throw new Exception();
    }

    try {
      temp = new Socket(server.accept());
    } catch (final Exception e) {
      throw new Exception();
    }

    temp.setPort(myPort);
    temp.setServer(myServer);

    return temp;
  }

  public void setPort(final int port)
  {
    myPort = port;
  }

  public void setServer(final String host)
  {
    myServer = host;
  }

  /********************************************************************
   * Socket I/O Functions
   ******************************************************************/

  public int send(final Packet packet) {
    return send(packet.toString());
  }
  public int send(final String s)
  {
    if (!init) return -1;
    else if (!serving)
    {
      if (client == null) return -2;
    }
    else if (server == null) return -3;

    write.println(s);
    if (write.checkError()) // If an error occured during sending, bail...
      return -4;

    return 0;
  }

  public String receive() throws Exception
  {
    String ret = null;
    if (!init)
      return ret;
    if (!serving)
    {
      if (client == null)
      {
        Log.err("cannot receive data: no I/O objects exist or the socket is null");
        return ret;
      }
    }
    else if (server == null) {
      Log.err("cannot receive data: no I/O objects exist or the socket is null");
      return ret;
    }

    try {
      ret = read.readLine();
    } catch (final Exception e)
    {
      Log.err("cannot receive data: no I/O objects exist or the socket is null");
      throw new Exception();
    }
    return ret;
  }
}
