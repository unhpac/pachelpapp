/**
 * The HelpApp Client GUI
 * 
 * @author Stephen Dunn
 */
package pac;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.TrayIcon.MessageType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;

import com.apple.eawt.AboutHandler;
import com.apple.eawt.AppEvent;
import com.apple.eawt.AppEvent.AboutEvent;
import com.apple.eawt.AppEvent.PreferencesEvent;
import com.apple.eawt.Application;
import com.apple.eawt.PreferencesHandler;
import com.apple.eawt.QuitHandler;
import com.apple.eawt.QuitResponse;

import pac.ClaimManager.CLAIM_STATE;
import pac.Packet.PACKET;

public class HelpApp extends JDialog {

  // general vars accessed outside this class
  private static final long  serialVersionUID = 7865217818779772824L;
  public static final String VERSION          = "0.6.0";
  public static HelpApp      self             = null;
  public static enum STATE {
    CLAIM_CREATE, CLAIM_EDIT, CLAIM_PENDING,
    CLAIM_CANCEL_PENDING, RECEIVING_HELP, CLAIM_FEEDBACK,
    PROGRAM_FEEDBACK, EXITING, UNKNOWN
  }
  public static Client             client; // network component
  private static Packet            toSend; // pending packet if we get disconnected
  private static Packet            oldClaim          = null; // old claim from last connection to server
  private static STATE             state             = STATE.CLAIM_EDIT; // tracks application state
  private static int               admin             = 0; // remote administrator? (not implemented yet)
  private static String            username, computer, firstname;
  private static String[]          args;
  private static ShutdownHook      catchJVMClose;

  // trayIcon data
  public static final String          ICON_PATH      = "resources/Icon.gif";
  public static Image                 iconImage;
  public static ImageIcon             imageIcon;
  private static TrayIcon             trayIcon;
  private static IconMenu             menu;
  private static SystemTray           tray;

  // GUI components
  private static final String      MSG_INVALID_CLAIM = "It appears that your claim is blank or "
      + "contains some unsendable characters. Please revise it and try again.";
  private static final String      MSG_BLANK_CLAIM   = "You must enter something to send.";
  private static final String      MSG_SEND_FAIL     = "Your request could not be sent at this time. "
      + "It will be automatically sent when the server comes back online.";
  private static final String      MSG_SEND_SUCCESS  = "Your request has been submitted for review.";
  private static final String      MSG_SEND_IGNORE   = "This request is still pending. We'll get to you as soon as possible.";
  private static final String      MSG_CANCELED      = "Your request was canceled successfully.";
  private static final String      MSG_NOT_CONNECTED = "I'm having some trouble connecting right now, but will keep retrying.";
  private static final String      MSG_THANKS        = "Thanks for using the HelpApp! Right-click the trayIcon and choose \"About\" to get feedback-related info.";
  private static final String      MSG_RECONNECTED   = "The HelpApp has re-established a connection to the server, and any pending requests have been sent.";
  private static final String      CAPTION           = "Submit a Help Request";
  private static final String[]    LABELS            = { "Send", "Cancel",
    "Send Edits", "Getting Helped", "Resolved" };
  private static final int         BTN_SEND          = 0;
  private static final int         BTN_CANCEL        = 1;
  private static final int         MIN_BTN_COUNT     = 2;
  private static final int         BTN_EDIT          = 2;
  private static final int         BTN_HELPING       = 3;
  private static final int         BTN_RESOLVED      = 4;
  private static final int         WIDTH             = 575, HEIGHT = 350;

  private static JTextArea         txtClaim;
  private static JTextField        txtConsultant;
  private static JScrollPane       scrollPane;
  private static JButton           button[], btnHelping;
  private static JPanel            panelMenu, panelTextArea;
  private static javax.swing.Timer tmrReconnect;
  private static boolean           editMode          = false, resolving = false;
  private static String            lastRequest       = "";

  // functions begin
  public HelpApp(final String[] args) throws NullPointerException {
    super((Frame)null, HelpApp.CAPTION); // this is important for the GUI to look appropriate

    synchronized (HelpApp.class) { // thread-safe singleton
      if (HelpApp.self != null) throw new NullPointerException();
    }
    HelpApp.self = this; // lame, fix later
    HelpApp.args = args; // unused for now, but good to have around for later

    if (!initTray()) { // initialize tray - critical for everything else
      Log.err("fatal trayIcon tray error");
      HelpApp.exit(-3);
    }

    /******************************************************************************
     * platform specific assistance
     **************************************************************************/

    if (Utils.getOS().equals("mac"))      new MACSupport();
    else if (Utils.getOS().equals("pc"))  { Utils.sleep(500); new PCSupport(); }
    else                                  { Utils.sleep(500); new LinuxSupport(); }

    /******************************************************************************
     * initialize log, gui, and network
     **************************************************************************/

    // on failure, silently revert to defaults:
    if (!Utils.loadConfig()) {
      Utils.updateConfigFile();
    }

    if (!Log.createLog(Utils.getClientLog()))
      Log.err("cannot read/write a log file"); // -> stderr

    Log.out("PACHelpApp (v" + HelpApp.VERSION + ") initializing...");

    if (!initUserInfo()) {
      Log.err("failed to obtain critical user info");
      HelpApp.exit(-1);
    }

    try { initNetwork(); } // don't care if this fails
    catch (Exception e) { }
    
    if (!initGUI()) { // initialize GUI
      Log.err("GUI creation failure");
      HelpApp.exit(-4);
    }

    // timer for reconnecting
    try {
      HelpApp.tmrReconnect = new javax.swing.Timer(Utils.getHeartbeatDisconnect() * 1000, new SendTimerListener());
      HelpApp.tmrReconnect.setInitialDelay(Utils.getHeartbeat() * 1000);
    } catch (final Exception e) {
      Log.err("failed to create the reconnection timer", true);
      HelpApp.exit(-5);
    }

    addWindowListener(new java.awt.event.WindowAdapter() {
      @Override
      public void windowIconified(final WindowEvent winEvt) {
        setVisible(false);
      }
    });

    addWindowListener(new java.awt.event.WindowAdapter() {
      @Override
      public void windowClosing(final WindowEvent winEvt) {
        setVisible(false); // we don't want to allow students to exit in the PAC
      }
    });

    // Catch JVM shutdown event
    try {
      HelpApp.catchJVMClose = new ShutdownHook();
      Runtime.getRuntime().addShutdownHook(HelpApp.catchJVMClose);
    } catch (final Exception e) {
      Log.err("couldn't create a shutdown hook on the JVM", true);
    }

    restoreClaim(); // this will throw no exceptions on failure
    Log.out("PACHelpApp initialized");
  }

  private static boolean initUserInfo() {
    // prompt for username on remote connections, otherwise auto-grab
    if (Utils.isRemoteConnection()) {

      HelpApp.username = Utils.promptForUsername();
      if (username == null) HelpApp.exit();

    } else {
      HelpApp.username = Utils.getUsername();
    }

    // user decided to cancel, time to quit
    if (HelpApp.username == null || HelpApp.username.length() < 1)
      HelpApp.exit();

    // determine machine
    HelpApp.computer = Utils.getHostname();
    if (HelpApp.computer == null) {
      Log.err("could not determine computer name");
      HelpApp.computer = Claim.UNKNOWN_VALUE;
    }

    // determine first name
    HelpApp.firstname = Utils.getFirstName();
    if (HelpApp.firstname == null) {
      Log.err("could not determine user's first name");
      HelpApp.firstname = new String(HelpApp.username); // reuse user name as first name
    }

    return true;
  }

  /******************************************************************************
   * Initialize Icon Tray
   **************************************************************************/
  // pop-menu for right-click on trayIcon
  static class IconMenu extends PopupMenu {
    /**
     * 
     */
    private static final long serialVersionUID = -4198315806436714572L;
    MenuItem                  itemQuit, itemAbout, itemReport, itemHelp;

    public IconMenu() {
      itemQuit = new MenuItem("Quit");
      itemQuit.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(final ActionEvent e) {
          HelpApp.exit();
        }
      });

      itemAbout = new MenuItem("About");
      itemAbout.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(final ActionEvent e) {
          Utils.aboutMsg(null, imageIcon);
        }
      });

      itemReport = new MenuItem("Report Bug");
      itemReport.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(final ActionEvent e) {
          Utils.reportBug();
        }
      });

      itemHelp = new MenuItem("Help!");
      itemHelp.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          if (HelpApp.client == null || !HelpApp.client.isConnected()) {
            trayIcon.displayMessage("Hmmm....", HelpApp.MSG_NOT_CONNECTED, MessageType.INFO);
            if (!initNetwork()) {
              HelpApp.toSend = null;
              HelpApp.tmrReconnect.start();
            } else {
              HelpApp.focus(); // reconnect worked!
            }
          } else {
            HelpApp.focus();
          }
        }
      });

      add(itemHelp);
      addSeparator();
      add(itemReport);
      add(itemAbout);
      add(itemQuit);
    }
  }

  public static boolean initTray() {

    Runnable runner = new Runnable() {
      @Override
      public void run() {
        if (!SystemTray.isSupported()) {
          Log.err("system tray is not supported on this OS");
          HelpApp.exit(-1);
        }

        try {
          iconImage = Utils.getImage(HelpApp.ICON_PATH);
          imageIcon = new ImageIcon(HelpApp.iconImage);
        } catch (final Exception e) {
          Log.err("missing or corrupt trayIcon file: " + ICON_PATH);
          HelpApp.exit(-1);
        } finally {
          if (iconImage == null || imageIcon == null) {
            Log.err("missing or corrupt trayIcon file: " + ICON_PATH);
            HelpApp.exit(-1);
          }
        }

        try {

          HelpApp.tray = SystemTray.getSystemTray();
          HelpApp.menu = new IconMenu();

          HelpApp.trayIcon = new TrayIcon(HelpApp.iconImage, "PAC Help App", HelpApp.menu);
          HelpApp.trayIcon.setImageAutoSize(true);
          HelpApp.trayIcon.setToolTip("PAC Help App v" + HelpApp.VERSION);

        } catch (Exception e) {
          Log.err("failed to initialize the menu");
          HelpApp.exit(-1);
        }

        try {
          HelpApp.tray.add(HelpApp.trayIcon);
        } catch (final Exception e) {
          Log.err("failed to add HelpApp to system tray");
          HelpApp.exit(-1);
        }
      }
    };

    EventQueue.invokeLater(runner);

    return true;
  }

  /******************************************************************************
   * Initialize GUI
   **************************************************************************/
  public boolean initGUI() {
    HelpApp.editMode = false;

    try {

      setVisible(false);
      setUndecorated(false);
      setResizable(false);
      setSize(HelpApp.WIDTH, HelpApp.HEIGHT);
      setLocationRelativeTo(getRootPane());
      setIconImage(iconImage);

    } catch (final Exception e) {
      Log.err("failed to initialize a frame");
      return false;
    }

    // Add everything to the JFrame
    try {
      HelpApp.button = new JButton[4];
      HelpApp.panelMenu = new JPanel(new GridLayout(0, 1));
      for (int i = 0; i < HelpApp.MIN_BTN_COUNT; i++) {
        HelpApp.button[i] = new JButton(HelpApp.LABELS[i]);
        HelpApp.button[i].setEnabled(false);
        HelpApp.button[i].setPreferredSize(new Dimension(130, 10));
        HelpApp.button[i].addActionListener(new ClaimListener(i));
        HelpApp.panelMenu.add(HelpApp.button[i]);
      }
      HelpApp.button[HelpApp.BTN_SEND].setEnabled(true);

      add(HelpApp.panelMenu, BorderLayout.EAST);

      HelpApp.txtClaim = new JTextArea("");
      HelpApp.scrollPane = new JScrollPane(HelpApp.txtClaim);
      HelpApp.scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
      HelpApp.scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
      add(HelpApp.scrollPane, BorderLayout.CENTER);

      final JPanel bottom = new JPanel(new GridLayout(1, 0));
      HelpApp.txtConsultant = new JTextField("");
      bottom.add(HelpApp.txtConsultant);
      HelpApp.btnHelping = new JButton(HelpApp.LABELS[HelpApp.BTN_HELPING]);
      HelpApp.btnHelping.setEnabled(false);
      HelpApp.btnHelping.addActionListener(new ClaimListener(HelpApp.BTN_HELPING));
      bottom.add(HelpApp.btnHelping);

      if (!Utils.isRemoteConnection() || Utils.inPAC(client.getIPString())) 
        add(bottom, BorderLayout.SOUTH);

    } catch (final Exception e) {
      Log.err("failed while adding components to GUI");
      e.printStackTrace();
      return false;
    }

    return true;
  }

  public static void focus() {
    HelpApp.self.setFocusable(true);
    HelpApp.self.setVisible(true);
    HelpApp.self.setLocationRelativeTo(null);
    HelpApp.self.toFront();
  }


  public static void setState(final STATE state) {
    HelpApp.state = state;
    switch (state) {

      case CLAIM_CREATE:
        HelpApp.self.setVisible(false);
        HelpApp.button[HelpApp.BTN_SEND].setEnabled(true);
        HelpApp.button[HelpApp.BTN_SEND].setText(HelpApp.LABELS[HelpApp.BTN_SEND]);
        HelpApp.button[HelpApp.BTN_CANCEL].setEnabled(false);
        HelpApp.btnHelping.setEnabled(false);
        HelpApp.btnHelping.setText(HelpApp.LABELS[HelpApp.BTN_HELPING]);
        HelpApp.editMode = false;
        break;

      case CLAIM_PENDING:
      case CLAIM_EDIT:
        HelpApp.self.setVisible(false);
        HelpApp.button[HelpApp.BTN_SEND].setText(HelpApp.LABELS[HelpApp.BTN_EDIT]);
        HelpApp.button[HelpApp.BTN_CANCEL].setEnabled(true);
        HelpApp.btnHelping.setEnabled(true);
        HelpApp.editMode = true;
        break;

      case CLAIM_CANCEL_PENDING:
        HelpApp.self.setVisible(false);
        HelpApp.button[HelpApp.BTN_SEND].setEnabled(false);
        HelpApp.button[HelpApp.BTN_CANCEL].setEnabled(true);
        HelpApp.btnHelping.setEnabled(false);

      case RECEIVING_HELP:

        HelpApp.self.setVisible(false);
        HelpApp.button[HelpApp.BTN_SEND].setEnabled(false);
        HelpApp.button[HelpApp.BTN_CANCEL].setEnabled(false);
        HelpApp.btnHelping.setText(HelpApp.LABELS[HelpApp.BTN_RESOLVED]);
        break;

      case PROGRAM_FEEDBACK:
        break;

      case EXITING:
        HelpApp.exit();
        break;

      default:
        Log.err("invalid state specified");
        HelpApp.setState(STATE.CLAIM_CREATE);

    }
  }

  public static STATE state() {
    return HelpApp.state;
  }

  public static void update() {
    HelpApp.setState(HelpApp.state);
  }

  public static boolean initNetwork() {

    synchronized(self) {
      try {

        //if (HelpApp.client != null && HelpApp.client.isConnected()) return false;
        HelpApp.client = new Client();

      } catch (final Exception e) {
        return false;
      }

      // connect to server
      Log.out("connecting to \"" + HelpApp.client.getServer() + ":" + HelpApp.client.getPort() + "\" as user \""
          + HelpApp.username + "\"...");

      boolean badName = true;
      while (badName) {
        try {
          if (HelpApp.client.connect(HelpApp.username, HelpApp.computer, HelpApp.firstname) != 0) {
            Log.err("failed to communicate with server \"" + HelpApp.client.getServer() + ":" + HelpApp.client.getPort() + "\".");
            return false;
          }

          Packet p = HelpApp.client.receive();
          if (p.is(PACKET.INVALID)) {
            Log.msg("The username \"" + username + "\" wasn't recognized.");

            HelpApp.username = null;
            while (Utils.isRemoteConnection() && HelpApp.username == null || HelpApp.username.length() < 1) {
              try { HelpApp.username = Utils.getUsername(); }
              catch (Exception e) { }
              if (HelpApp.username == null)
                HelpApp.exit();
            }

          } else badName = false;

          if (!Utils.isRemoteConnection() && badName) {
            Log.err("username not recognized: \"" + username);
            HelpApp.exit();
          }

        } catch (final Exception e) {
          Log.err("internal connection failure for server: \""
              + HelpApp.client.getServer() + ":" + HelpApp.client.getPort() + "\"");
          return false;
        }
      }

      if (badName == true && !Utils.isRemoteConnection()) {
        Log.out("username \"" + username + "\" not recognized by the server");
        HelpApp.exit();
      }

      // receive old claim (just a heartbeat on new connection)
      try {
        HelpApp.oldClaim = HelpApp.client.receive();
      } catch (final Exception e) {
        Log.err("connection to server \"" + HelpApp.client.getServer() + ":" + HelpApp.client.getPort() + "\" failed");
        return false;
      }

      Log.out("connected to server \"" + HelpApp.client.getServer() + ":" + HelpApp.client.getPort() + "\"");
    }

    return true;
  }

  public static void restoreClaim() {
    if (HelpApp.oldClaim == null) return;
    try {
      final String s = HelpApp.oldClaim.getData();
      final String[] parts = s.split(ClaimManager.claimPartSeparatorSymbol);
      if (!parts[Claim.STATE_INDEX].equals(CLAIM_STATE.RESOLVED)) {
        HelpApp.txtClaim.setText(parts[Claim.CLAIM_INDEX]);
        HelpApp.lastRequest = parts[Claim.CLAIM_INDEX];
        HelpApp.setState(STATE.CLAIM_EDIT);

        if (Utils.isRemoteConnection()) HelpApp.focus();
      }
    } catch (final Exception e) {
      return;
    }
    Log.out("claim restored from server");
  }

  public void cancelClaim() {
    HelpApp.toSend = new Packet(PACKET.CANCEL);
    final int ret = HelpApp.client.send(HelpApp.toSend);
    if (ret != 0) {
      HelpApp.tmrReconnect.start();
      Log.msg(HelpApp.MSG_SEND_FAIL);
      return;
    } else {
      HelpApp.setState(STATE.CLAIM_CREATE);
      setVisible(false);
      Log.msg(HelpApp.MSG_CANCELED);
      return;
    }
  }

  public void editClaim() {
    HelpApp.lastRequest = HelpApp.txtClaim.getText().trim(); // update the last sent message
    HelpApp.toSend = new Packet(PACKET.EDIT, HelpApp.lastRequest);
    final int ret = HelpApp.client.send(HelpApp.toSend);

    setVisible(false);
    if (ret != 0) {
      HelpApp.setState(STATE.CLAIM_PENDING);
      HelpApp.tmrReconnect.start();
      Log.msg(HelpApp.MSG_SEND_FAIL);
      return;
    } else Log.msg(HelpApp.MSG_SEND_SUCCESS);
  }

  public void sendClaim() {
    HelpApp.toSend = new Packet(PACKET.CLAIM, HelpApp.lastRequest);
    final int ret = HelpApp.client.send(HelpApp.toSend);

    setVisible(false);
    if (ret != 0) { // failed to send, begin retry timer
      HelpApp.setState(STATE.CLAIM_PENDING);
      HelpApp.tmrReconnect.start();
      Log.msg(HelpApp.MSG_SEND_FAIL);
    } else {
      HelpApp.setState(STATE.CLAIM_EDIT);
      Log.msg(HelpApp.MSG_SEND_SUCCESS);
    }
  }

  public void resolvingClaim() {
    final String cons = HelpApp.txtConsultant.getText().trim();
    if (cons == null || cons.equals("")) {
      Log.msg("A valid consultant username must be entered.");
      return;
    }

    HelpApp.toSend = new Packet(PACKET.HELPING, cons);
    final int ret = HelpApp.client.send(HelpApp.toSend);

    setVisible(false);
    if (ret != 0) { // failed to send, begin retry timer
      HelpApp.setState(STATE.CLAIM_PENDING);
      HelpApp.tmrReconnect.start();
      Log.msg(HelpApp.MSG_SEND_FAIL);
    } else {
      HelpApp.resolving = true;
      HelpApp.setState(STATE.RECEIVING_HELP);
    }
  }

  public void resolvedClaim() {
    final String cons = HelpApp.txtConsultant.getText().trim();
    if (cons == null || cons.equals("")) {
      Log.msg("A valid consultant username must be entered.");
      return;
    }

    HelpApp.toSend = new Packet(PACKET.RESOLVED, cons);
    if (HelpApp.client.send(HelpApp.toSend) != 0) {
      Log.msg(HelpApp.MSG_SEND_FAIL);
      return;
    }

    setVisible(false);
    HelpApp.txtClaim.setText("");
    HelpApp.lastRequest = "";
    HelpApp.resolving = false;
    HelpApp.setState(STATE.CLAIM_CREATE);

    Log.msg(HelpApp.MSG_THANKS);
  }

  public static int getAdmin() {
    return HelpApp.admin;
  }

  public static void resetGUI() {
    HelpApp.button[HelpApp.BTN_SEND].setText(HelpApp.LABELS[HelpApp.BTN_SEND]);
    HelpApp.button[HelpApp.BTN_SEND].setEnabled(true);
    HelpApp.button[HelpApp.BTN_CANCEL].setText(HelpApp.LABELS[HelpApp.BTN_CANCEL]);
    HelpApp.button[HelpApp.BTN_CANCEL].setEnabled(false);

    HelpApp.txtClaim.setText("");
    HelpApp.txtConsultant.setText("");

    HelpApp.editMode = false;
  }

  public static void exit() {
    HelpApp.exit(0);
  }

  public static void exit(final int exitCode) {

    try {
      HelpApp.tray.remove(HelpApp.trayIcon);
    } catch (final Exception e) {}

    try {
      if (HelpApp.client != null) HelpApp.client.disconnect();
    } catch (final Exception e) {}

    // remove JVM hook if there is one
    try {
      if (HelpApp.catchJVMClose != null) Runtime.getRuntime().removeShutdownHook(HelpApp.catchJVMClose);
    } catch (final Exception e) {
      // we do not log this as an error because an exception will always occur if we try to unhook
      // during the execution of a shutdown hook. there are 3 ways this program can exit:
      // 1. The JVM shuts down gracefully - then we get here WITH an exception, but it's okay
      // 2. Abnormal/Forced kill - we do not get here ever, nothing in log will suggest bad exit
      // 3. User quits/Server remote terminates - graceful quit, we should not get here.
    }

    try {
      if (exitCode == 0) Log.out("PACHelpApp terminated properly");
      else Log.err("PACHelpApp terminated with code: " + exitCode);

      Log.closeLog();
    } catch (final Exception e) {
      Log.err("internal failure during exit");
    }

    System.exit(exitCode);
  }

  public static void fatalError() {
    Log.err("a fatal error has occured.\nthe application state was \""
        + HelpApp.state.toString() + "\" prior to the crash");
    HelpApp.exit(-1);
  }

  class ShutdownHook extends Thread {
    @Override
    public void run() {
      Log.debugLog("JVM shutdown detected...");
      HelpApp.setState(HelpApp.STATE.EXITING);
    }
  }

  /***********************************************************************
   * GUI Response
   *******************************************************************/

  public class ClaimListener implements ActionListener {
    int buttonId;

    public ClaimListener(final int buttonId) {
      this.buttonId = buttonId;
    }

    @Override
    public void actionPerformed(final ActionEvent ev) {
      switch (buttonId) {
        case BTN_SEND: // send the current claim
        {
          if (HelpApp.client == null) {
            Log.err("server was diconnected at send time, attempting reconnect");
            HelpApp.tmrReconnect.start();
            Log.msg(HelpApp.MSG_SEND_FAIL);
            return;
          }

          if (!HelpApp.editMode) { // send a new request (not in edit mode)
            if (HelpApp.txtClaim.getText() == null || HelpApp.txtClaim.getText().length() <= 0) {
              Log.msg(HelpApp.MSG_BLANK_CLAIM);
              return; // Don't send a null claim
            }

            // chop whitespace and validate
            String claimText = null;
            try {
              claimText = HelpApp.txtClaim.getText().trim();
              if (claimText == null || claimText.length() <= 0) {
                Log.msg(HelpApp.MSG_INVALID_CLAIM);
                return;
              }

            } catch (final Exception e) {
              Log.err("internal error");
              Log.msg(HelpApp.MSG_INVALID_CLAIM);
              return;
            }

            // finally time to attempt a send
            HelpApp.lastRequest = claimText; // store so edits aren't redundant
            HelpApp.self.sendClaim();
            return;

          } else { // we're in edit mode

            final String txt = HelpApp.txtClaim.getText();
            if (txt == null || txt.trim().length() <= 0) {

              final int n = JOptionPane.showConfirmDialog(
                  HelpApp.self,
                  "Do you want to cancel your claim?",
                  "Cancel Claim?",
                  JOptionPane.YES_NO_OPTION);

              if (n == JOptionPane.YES_OPTION) HelpApp.self.cancelClaim();
              else {
                Log.msg(HelpApp.MSG_INVALID_CLAIM);
                return;
              }

            } else if (txt.trim().equals(HelpApp.lastRequest)) {
              Log.msg(HelpApp.MSG_SEND_IGNORE);
              return; // don't send redundant edits
            }

            HelpApp.self.editClaim();
          }
        }
        break;

        // cancel claim / nevermind
        case BTN_CANCEL: {
          HelpApp.setState(STATE.CLAIM_CREATE);
          setVisible(false);
          HelpApp.self.cancelClaim();
        }
        break;

        case BTN_HELPING: { // Getting helped
          if (HelpApp.resolving) HelpApp.self.resolvedClaim();
          else HelpApp.self.resolvingClaim();
        }
        break;

        default:
          Log.err("an invalid option was passed to the HelpApp");
      }
    }
  }

  /*************************************************************************
   * Timer for retrying a failed message attempt
   *********************************************************************/

  public class SendTimerListener implements ActionListener {
    private final int sendFailCount = 0;

    public SendTimerListener() {}

    @Override
    public void actionPerformed(final ActionEvent e) {
      // client object is dead, try to recreate
      if (HelpApp.client == null || !HelpApp.client.isConnected()) {
        if (!HelpApp.initNetwork()) return; // failed to reconnect
        HelpApp.trayIcon.displayMessage("Reconnected!", HelpApp.MSG_RECONNECTED, MessageType.INFO);
      }

      if (HelpApp.toSend == null) { // null data means bail out
        HelpApp.tmrReconnect.stop();
        return;
      }

      // everything's normal, try sending:
      final int ret = HelpApp.client.send(HelpApp.toSend);
      if (ret == 0) { // success!
        HelpApp.toSend = null;
        HelpApp.tmrReconnect.stop();
        return;
      }
    }
  }

  /*************************************************************************
   * linux support
   *********************************************************************/
  public class LinuxSupport {
    public LinuxSupport() {
      HelpApp.trayIcon.addMouseListener(new MouseAdapter() {
        @Override
        public void mousePressed(MouseEvent e) {
          if (e.getClickCount() == 1) {
            if (SwingUtilities.isLeftMouseButton(e)) {
              for(ActionListener a: HelpApp.menu.itemHelp.getActionListeners()) {
                a.actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null) {
                  private static final long serialVersionUID = -1842452561118019400L;
                });
              }
            }
          }
        }
      });
    }
  }

  /*************************************************************************
   * pc support
   *********************************************************************/
  public class PCSupport {
    public PCSupport() {
      HelpApp.trayIcon.addMouseListener(new MouseAdapter() {
        @Override
        public void mousePressed(MouseEvent e) {
          if (e.getClickCount() == 1) {
            if (SwingUtilities.isLeftMouseButton(e)) {
              for(ActionListener a: HelpApp.menu.itemHelp.getActionListeners()) {
                a.actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null) {
                  private static final long serialVersionUID = -1991991266469390198L;
                });
              }
            }
          }
        }
      });
    }
  }

  /*************************************************************************
   * mac os x support via OrangeExtensions
   *********************************************************************/
  public class MACSupport implements QuitHandler, AboutHandler, PreferencesHandler {
    //mac support
    //for info on the properties i set below, see:
    //http://developer.apple.com/library/mac/#documentation/Java/Reference/Java_PropertiesRef/Articles/JavaSystemProperties.html
    //for menu handlers, see apple developer reference manual

    public MACSupport() {
      System.setProperty("com.apple.mrj.application.apple.menu.about.name", "PACHelpApp");
      System.setProperty("apple.laf.useScreenMenuBar", "true"); // add to menu bar
      System.setProperty("apple.laf.UIElement", "true"); // hide from dock

      Application me = Application.getApplication();
      me.enableSuddenTermination();
      me.setQuitHandler(this);
      me.setAboutHandler(this);
      me.setPreferencesHandler(this);
      me.setDockIconImage(iconImage);
      me.setDockMenu(menu);

      Utils.defaultConfigPath = "/Library/Application Support/PACHelpApp/";
    }

    @Override
    public void handleQuitRequestWith(AppEvent.QuitEvent e, QuitResponse response) {
      response.performQuit();
      HelpApp.exit();
    }

    @Override
    public void handleAbout(AboutEvent e) {
      try {
        Utils.aboutMsg(null, imageIcon);
      } catch (Exception ex)  {}
    }

    @Override
    public void handlePreferences(PreferencesEvent e) {}

  }

  @SuppressWarnings("unused")
  public static void main(final String[] args) {
    try {
      final HelpApp app = new HelpApp(args);
    } catch (final Exception e) {
      System.err.println("failed to initialize the HelpApp - am I already running?\n");
      e.printStackTrace();
    }
  }
}
