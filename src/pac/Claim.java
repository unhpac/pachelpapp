package pac;

import pac.ClaimManager.CLAIM_STATE;

public class Claim {
  public static final String UNKNOWN_VALUE    = "Unknown";
  public String              claim            = "",
      timeStarted = CLAIM_STATE.UNRESOLVED.name(),
      timeFinished = CLAIM_STATE.UNRESOLVED.name(),
      dateStarted = CLAIM_STATE.UNRESOLVED.name(),
      dateFinished = CLAIM_STATE.UNRESOLVED.name(),
      username = Claim.UNKNOWN_VALUE, firstName = Claim.UNKNOWN_VALUE,
      consultant = Claim.UNKNOWN_VALUE, groups = Claim.UNKNOWN_VALUE,
      computer = Claim.UNKNOWN_VALUE;

  public static final int    CLAIM_INDEX      = 0;
  public static final int    ID_INDEX         = 1;
  public static final int    FIRSTNAME_INDEX  = 2;
  public static final int    USERNAME_INDEX   = 3;
  public static final int    GROUPS_INDEX     = 4;
  public static final int    COMPUTER_INDEX   = 5;
  public static final int    TIME_START_INDEX = 6;
  public static final int    DATE_START_INDEX = 7;
  public static final int    TIME_END_INDEX   = 8;
  public static final int    DATE_END_INDEX   = 9;
  public static final int    STATE_INDEX      = 10;
  public static final int    SUGGESTION_INDEX = 11;

  public SuggestionManager  suggestions;
  public CLAIM_STATE        state            = CLAIM_STATE.UNRESOLVED;
  private int                id               = -1;

  public static class ClaimBuilder {
    protected Claim claim;

    public ClaimBuilder() {
      claim = new Claim();
    }

    public ClaimBuilder id(final int id) {
      claim.id = id;
      return this;
    }

    public ClaimBuilder claim(final String claim) {
      this.claim.claim = new String(claim);
      return this;
    }

    public ClaimBuilder timeStarted(final String started) {
      claim.timeStarted = new String(started);
      return this;
    }

    public ClaimBuilder timeFinished(final String finished) {
      claim.timeFinished = new String(finished);
      return this;
    }

    public ClaimBuilder dateStarted(final String dateStarted) {
      claim.dateStarted = new String(dateStarted);
      return this;
    }

    public ClaimBuilder dateFinished(final String dateFinished) {
      claim.dateFinished = new String(dateFinished);
      return this;
    }

    public ClaimBuilder username(final String username) {
      claim.username = new String(username);
      return this;
    }

    public ClaimBuilder firstName(final String firstName) {
      claim.firstName = new String(firstName);
      return this;
    }

    public ClaimBuilder consultant(final String consultant) {
      claim.consultant = new String(consultant);
      return this;
    }

    public ClaimBuilder groups(final String groups) {
      claim.groups = new String(groups);
      return this;
    }

    public ClaimBuilder computer(final String computer) {
      claim.computer = new String(computer);
      return this;
    }

    public ClaimBuilder creationDateAndTime(final String creationDateAndTime) {
      try {
        final String[] parts = creationDateAndTime.split(" ");
        claim.timeStarted = parts[1];
        claim.dateStarted = parts[0];
      } catch (final Exception e) {
        Log.err("invalid date & time string received: "
            + creationDateAndTime);
      }

      return this;
    }

    public ClaimBuilder finishDateAndTime(final String finishDateAndTime) {
      try {
        final String[] parts = finishDateAndTime.split(" ");
        claim.timeFinished = parts[1];
        claim.dateFinished = parts[0];
      } catch (final Exception e) {
        claim.timeFinished = "Unresolved";
        claim.dateFinished = "Unresolved";
      }

      return this;
    }

    public ClaimBuilder claimState(final CLAIM_STATE claimState) {
      claim.state = claimState;
      return this;
    }

    public ClaimBuilder suggestions(final String loadedSuggestions) {
      claim.suggestions.loadFromString(loadedSuggestions);
      return this;
    }

    public Claim build() {
      if (claim.id == -1) {
        Log.err("cannot create claim - no claim id was specified");
        return null;
      }
      return claim;
    }
  }

  /*****************************************************************
   * Claim begins
   ****************************************************************/
  private Claim() throws NullPointerException {
    try {
      suggestions = new SuggestionManager();
    } catch (final Exception e) {
      Log.err("failure instantiating a suggestion manager");
      throw new NullPointerException();
    }
  }

  public Claim(final String loadString) throws NullPointerException {
    if (!loadFromString(loadString)) throw new NullPointerException();
  }

  public CLAIM_STATE state() {
    return state;
  }
  public void setState(final CLAIM_STATE new_state) {
    state = new_state;
    if (state == CLAIM_STATE.RESOLVED || state == CLAIM_STATE.CANCELED) {
      timeFinished = Utils.getTime();
      dateFinished = Utils.getDate();
    }
  }

  public void attachSuggestion(final String consultant, final String suggestion,
                               final String dateTime) {
    suggestions.add(suggestion, consultant, dateTime);
    Log.out("New suggestion from \"" + consultant + "\" attached to claim id "
        + id + " at " + dateTime + ":\n" + suggestion);
  }

  public void finish(final String who) {
    setState(CLAIM_STATE.RESOLVED);
    consultant = who;
  }

  public void updateClaim(final String newClaim) {
    System.out.println("Updating from \"" + claim + "\" to \"" + newClaim
        + "\"");
    claim = newClaim;
  }

  public int id() {
    return id;
  }

  @Override
  public String toString() {
    try {

      final String s = claim
          + ClaimManager.claimPartSeparatorSymbol + id()
          + ClaimManager.claimPartSeparatorSymbol + firstName
          + ClaimManager.claimPartSeparatorSymbol + username
          + ClaimManager.claimPartSeparatorSymbol + groups
          + ClaimManager.claimPartSeparatorSymbol + computer
          + ClaimManager.claimPartSeparatorSymbol + timeStarted
          + ClaimManager.claimPartSeparatorSymbol + dateStarted
          + ClaimManager.claimPartSeparatorSymbol + timeFinished
          + ClaimManager.claimPartSeparatorSymbol + dateFinished
          + ClaimManager.claimPartSeparatorSymbol + state.name()
          + ClaimManager.claimPartSeparatorSymbol + suggestions.toString();

      return s;

    } catch (final Exception e) {
      Log.err("failed converting claim to string");
    }

    return null;
  }

  public boolean loadFromString(final String claimString) {
    final String[] parts = claimString.split(ClaimManager.claimPartSeparatorSymbol);

    if (parts.length != 12) {
      Log.err("incorrect # of arguments passed. Received "
          + parts.length
          + ", was expecting 12, data received: "
          + claimString);
      return false;
    }

    try {
      suggestions = new SuggestionManager();
      if (!suggestions.loadFromString(parts[Claim.SUGGESTION_INDEX])) {
        Log.err("failed to load suggestions attached to claim "
            + id);
        return false;
      }
    } catch (final Exception e) {
      Log.err("loading suggestions");
      return false;
    }

    claim = parts[Claim.CLAIM_INDEX];

    try {
      id = Integer.parseInt(parts[Claim.ID_INDEX]);
    } catch (final Exception e) {
      Log.err("invalid claim id detected during load: "
          + parts[Claim.ID_INDEX]);
      return false;
    }

    firstName = parts[Claim.FIRSTNAME_INDEX];
    username = parts[Claim.USERNAME_INDEX];
    groups = parts[Claim.GROUPS_INDEX];
    computer = parts[Claim.COMPUTER_INDEX];
    timeStarted = parts[Claim.TIME_START_INDEX];
    dateStarted = parts[Claim.DATE_START_INDEX];
    timeFinished = parts[Claim.TIME_END_INDEX];
    dateFinished = parts[Claim.DATE_END_INDEX];

    try {
      state = CLAIM_STATE.valueOf(parts[Claim.STATE_INDEX].toUpperCase());
    } catch (final Exception e) {
      Log.err("invalid claim state specified: "
          + parts[Claim.STATE_INDEX]);
      state = CLAIM_STATE.ERROR;
      return false;
    }

    return true;
  }

}
