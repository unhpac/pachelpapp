package pac;

/**
 * Creates a PACServer listening (by default) on port 10218. Valid connections from PAC
 * clients come from range: 132.177.4.*
 * 
 * @author Stephen Dunn
 */

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import pac.ClaimManager.CLAIM_STATE;
import pac.Packet.PACKET;
import pac.Utils.CONFIG_VAR;

public class Server extends Thread {

  // private objects
  private Socket                     socket                = null;
  private Integer                    port                  = new Integer(CONFIG_VAR.PORT.toString());
  private final ArrayList<ServerThread> clientList;
  private boolean                    listening             = false;
  private final ClaimManager         claims;
  private final String               CONSOLE_COLUMN_FORMAT = "%-15.15s";

  //@formatter:off
  //shared
  public static final String usage = "\n>Welcome to the PAC Help Server (v"
      + pac.HelpApp.VERSION
      + ")\n\n"
      + "\tUsage: Server [switch] [options]\n"
      + "\tThe default port is currently set to "
      + CONFIG_VAR.PORT.toString()
      + ".\n\n"
      + "\tSwitch   Description\n"
      + "\tq\t Quit this application (stops server).\n"
      + "\ts [port] Start the server on [port]. Default if none specified.\n"
      + "\tr [port] Restart the server.\n"
      + "\tx\t Stop the server.\n"
      + "\tu\t Print a list of connected users.\n"
      + "\tg\t Print a list of connected user groups.\n"
      + "\tgd\t Print a list of user groups for all claims in the current database.\n"
      + "\tn\t Get number of connected clients.\n"
      + "\tp\t Print a summary of all open claims.\n"
      + "\tv [id]\t Print detailed information for claim [id].\n"
      + "\tc [id]\t Cancel claim [id]. Use \"p\" or \"v\" to view claims.\n"
      + "\ta [file] Save all claims to [filename]. Default if none specified.\n"
      + "\tl [file] Load claims from [filename]. Default if none specified.\n"
      + "\t?\t Print the usage statement.\n";
//@formatter:on

  /********************************************************************
   * Server Constructors
   ******************************************************************/

  private Server() { // use factory method to instantiate
    clientList = new ArrayList<ServerThread>();
    claims = new ClaimManager();

    if (!claims.load()) Log.out("Load failed, starting new database.");
    else Log.out("Loaded " + claims.size() + " claims from database \"" + ClaimManager.claimDatabase + "\".");
  }

  /********************************************************************
   * Server Functions
   ******************************************************************/

  @Override
  public void run() {
    try {
      socket = new Socket(port);
    } catch (final Exception e) {
      Log.err("failed to create a socket on port " + port);
      return;
    }

    if (socket == null) {
      Log.err("failed to initialize the server socket on port " + port);
      return;
    }

    Log.out("Listening on port " + port + ". Type '?' for help.");
    listening = true;

    while (listening && socket != null)
      try {
        final ServerThread temp = new ServerThread(socket.accept());
        clientList.add(temp);
        temp.start();
      } catch (final Exception e) {
      } // reported in ServerThread.run()
  }

  public void stopServer() {
    disconnectAllClients(); // Disconnect all clients
    disconnect(); // Close the hosting socket

    Log.out("The PAC Server has been stopped.");
  }

  public void save() {
    Log.out("Backing up database...");
    claims.save(ClaimManager.claimDatabase + ".bak");
    Log.out("Saving new claims database...");
    claims.save();
  }

  public String getConsoleFormat(final int columns) {
    String ret = "";
    for (int i = 0; i < columns; i++)
      ret += CONSOLE_COLUMN_FORMAT + "  ";
    return ret;
  }

  public void setPort(final int port) {
    this.port = new Integer(port);
  }
  public int port() {
    return port;
  }

  public static Server createServer() {
    return Server.createServer(Utils.getDefaultPort());
  }

  public static Server createServer(final int port) {
    final Server temp = new Server();

    // start new thread for accepting connections
    temp.setPort(port);
    try {
      temp.start();
    } catch (final Exception e) {
      Log.err("failed to start the server");
      return null;
    }

    return temp;
  }

  public static int portFromArgs(final String[] args) {
    int port = Utils.getDefaultPort();

    if (args.length > 1) try {
      port = new Integer(args[1].trim());
    } catch (final Exception e) {
      Log.err("invalid port specified on command line: "
          + args[1]);
      return -1;
    }

    return port;
  }

  public int getNumUsers() {
    int num = 0;
    final ArrayList<ServerThread> deadClients = new ArrayList<ServerThread>();
    for (final ServerThread i : clientList)
      if (i.ping()) num++;
      else deadClients.add(i);
    for (final ServerThread i : deadClients)
      clientList.remove(i);

    return num;
  }

  public String[] getUsers() {
    if (clientList == null)
      return null;

    int size = clientList.size();
    if (size <= 0) return null;

    final String[] names = new String[size];
    try {
      for (int i = 0; i < size; i++)
        if (clientList.get(i).ping())
          names[i] = clientList.get(i).getUsername();
    } catch (final Exception e) {
      Log.err("user retrieval failed");
    }
    return names;
  }

  public String[] getGroups() {
    if (clientList == null || clientList.size() <= 0)
      return null;

    final HashSet<String> groups = new HashSet<String>();
    for (final ServerThread c : clientList) {
      final String temp = c.groups;
      String[] parts = null;
      try {
        parts = temp.split(" ");
      } catch (final Exception e) {
        parts = null;
      }
      if (parts != null && parts.length > 1) for (final String s : parts)
        groups.add(s);
      else groups.add(new String(temp));
    }

    try {
      final String[] ret = new String[groups.size()];
      groups.toArray(ret);
      return ret;
    } catch (final Exception e) {}

    return null;
  }

  public String[] getGroupsFromDatabase() {
    if (claims.size() <= 0)
      return null;

    final HashSet<String> groups = new HashSet<String>();
    final ArrayList<Claim> claimList = claims.getClaims();
    for (final Claim c : claimList) {
      final String temp = c.groups;
      String[] parts = null;
      try {
        parts = temp.split(" ");
      } catch (final Exception e) {
        parts = null;
      }
      if (parts != null && parts.length > 1) for (final String s : parts)
        groups.add(s);
      else groups.add(new String(temp));
    }

    try {
      final String[] ret = new String[groups.size()];
      groups.toArray(ret);
      return ret;
    } catch (final Exception e) {}

    return null;
  }

  /*
   * public ArrayList<String> getClaimStrings() { ArrayList<String> temp = claims.getClaimStrings(); int i = 0; for (String s
   * : temp) { if (i > clientList.size()-1 || i < 0) { err(
   * "PACServer::getClaimStrings: Number of claims exceeds number of clients." ); break; } if (s != null) s =
   * clientList.get(i).getUserName() + ": \n" + s; i++; } return temp; }
   */

  public String getClaim(final int i) {
    if (i > claims.size() - 1 || i < 0) return null;
    return claims.getClaimString(i);
  }

  public void attachSuggestion(final String consultant, final int id, final String suggestion) {
    claims.attachSuggestion(consultant, id, suggestion);
    final ServerThread temp = getClientByClaimId(id);
    if (temp != null) temp.client.sendSuggestion(id, consultant, temp.getUsername(), suggestion);
    else Log.err("cannot communicate suggestion to necessary client");
  }

  public ServerThread getClientByClaimId(final int id) {
    for (final ServerThread temp : clientList)
      if (temp.claimId() == id)
        return temp;
    Log.err("no client found with id " + id);
    return null;
  }

  public void disconnectAllClients() {
    final int numUsers = getNumUsers();
    if (numUsers <= 0)
      return;

    Log.out("Disconnecting all clients (" + numUsers + ")" + "...");
    for (int i = 0; i < clientList.size(); i++) {
      clientList.get(i).killThread(); // Tell each thread to die by
      // raising an exception
      try {
        clientList.get(i).join();
      } catch (final Exception e) {} // Wait for each thread to terminate
      Log.out("Client \"" + clientList.get(i).getUsername()
          + "\" has been disconnected.");
    }

    clientList.clear();
  }

  public void disconnect() {
    if (socket != null) try {
      socket.close();
      socket = null;
    } catch (final Exception e) {
      Log.err("failed to terminate the hosting socket properly");
    }
    listening = false;
  }

  public void kickUser(final int index) {
    Log.out("Kicking user @ index " + index);
    clientList.get(index).disconnect();
  }

  public void kickUser(final ServerThread s) {
    Log.out("Kicking user @ thread " + s);
    for (int i = 0; i < clientList.size(); i++)
      if (clientList.get(i) == s) {
        clientList.get(i).disconnect();
        break;
      }
  }

  public Server interpretArgs(final String[] args, final Server server) {
    if (args.length <= 0) {
      Log.out(Server.usage);
      return null;
    }

    args[0] = args[0].toLowerCase();
    if (args[0].equals("x")) server.stopServer();
    else if (args[0].equals("s")) // start server on [port] *****************************
    {
      if (server.listening) {
        Log.out("There is already a server running. Use 'x' to stop the current server before starting a new one.");
        return null;
      }

      final int port = Server.portFromArgs(args);
      if (port != -1)
        return Server.createServer(port);
      return Server.createServer();
    } else if (args[0].equals("r")) // start server on [port] *****************************
    {
      if (server.listening == false) {
        Log.out("There is no server currently running.");
        return null;
      }

      server.stopServer();

      final int port = Server.portFromArgs(args);
      if (port != -1)
        return Server.createServer(port);
      return Server.createServer(server.port()); // use previous port for new server
    } else if (args[0].equals("n")) // # clients requested *****************************
    {
      if (server.listening) Log.out("Number of connected users: " + server.getNumUsers());
      else {
        Log.out("The PAC Server has not been started. Use '?' for help.");
        return null;
      }
    } else if (args[0].equals("u")) // users requested *****************************
    {
      if (server.listening) {
        if (server.getNumUsers() != 0) {
          final String[] temp = server.getUsers();
          Log.out("All connected users:");
          for (String element : temp)
            Log.out(element);
        } else {
          Log.out("There are no clients currently connected.");
          return null;
        }
      } else {
        Log.out("There is no server running on this machine.");
        return null;
      }
    } else if (args[0].equals("g")) // groups requested *****************************
    {
      if (server.clientList.size() == 0) {
        Log.out("There are no connected clients on this server.");
        return null;
      }
      final String[] g = getGroups();
      String msg = "";
      if (g != null) {
        for (int i = 0; i < g.length; i++) {
          msg += g[i] + " ";
          if (i % 10 == 0 && i != 0) msg = msg.trim() + "\n";
        }
        Log.out("All connected user groups:\n" + msg);
      } else Log.out("There are no groups available for the connected clients.");
    } else if (args[0].equals("gd")) { // groups from database requested *****************************
      if (claims.size() == 0) {
        Log.out("There are no claims available in database \"" + ClaimManager.claimDatabase + "\".");
        return null;
      }
      final String[] g = getGroupsFromDatabase();
      String msg = "";
      if (g != null) {
        for (int i = 0; i < g.length; i++) {
          msg += g[i] + " ";
          if (i % 10 == 0 && i != 0) msg = msg.trim() + "\n";
        }
        Log.out("All user groups in database \"" + ClaimManager.claimDatabase + "\":\n" + msg);
      } else Log.out("There are no groups available for the database \"" + ClaimManager.claimDatabase + "\".");
    } else if (args[0].equals("p")) { // print claim summary *****************************
      if (claims.size() <= 0) {
        Log.out("There are no claims open on this server.");
        return null;
      }
      Log.printf(getConsoleFormat(5) + "\n",
          "Claim ID", "Username", "State", "Time Started", "Time Finished");
      final String[] temp = claims.getClaimStrings();

      if (temp == null) {
        Log.err("an error occured retrieving claims from database \"" + ClaimManager.claimDatabase + "\"");
        return null;
      }
      for (String element : temp)
        try {

          final String[] cur = element.split(ClaimManager.claimPartSeparatorSymbol);
          Log.printf(getConsoleFormat(5) + "\n",
              cur[Claim.ID_INDEX], cur[Claim.USERNAME_INDEX],
              cur[Claim.STATE_INDEX], cur[Claim.TIME_START_INDEX],
              cur[Claim.TIME_END_INDEX]);

        } catch (final Exception e) {
          Log.err("an invalid claim string was retrieved from the manager");
          return null;
        }
    } else if (args[0].equals("v")) { // view claim [id] *****************************

      if (args.length < 2) {
        Log.out("You must specify a claim id. Use \"p\" to print all open claims.");
        return null;
      }
      try {
        final int id = Integer.parseInt(args[1]);
        final Claim temp = claims.getClaimById(id);
        final String format = getConsoleFormat(5) + "\n";
        Log.printf(format, "Username", "Firstname", "Group(s)", "Consultant", "Computer");
        Log.printf(format, temp.username, temp.firstName, temp.groups, temp.consultant, temp.computer);
        Log.printf(format, "Claim State", "Time Started", "Time Finished", "Date Started", "Date Finished");
        Log.printf(format, temp.state().name(), temp.timeStarted, temp.timeFinished, temp.dateStarted, temp.dateFinished);
        Log.out("Full Claim Submitted:\n" + temp.claim);

      } catch (final Exception e) {
        Log.out(Server.usage);
        return null;
      }

    } else if (args[0].equals("c")) // cancel claim [id] *****************************
    {

      if (args.length < 2) {
        Log.out("You must specify a claim id. Use \"p\" to print all open claims.");
        return null;
      }
      try {
        final int id = Integer.parseInt(args[1]);
        claims.cancel(id);
        Log.out("Claim " + id + " has been successfully canceled.");
      } catch (final Exception e) {
        Log.out(Server.usage);
        return null;
      }

    } else if (args[0].equals("a")) // save claims to [file] *****************************
    {
      String saveTo = ClaimManager.claimDatabase;
      if (args.length >= 2)
        saveTo = args[1].trim();

      Log.out("Saving claims to \"" + args[1].trim() + "\"...");
      claims.save(saveTo);

    } else if (args[0].equals("l")) // load claims from file *****************************
    {
      String loadFrom = ClaimManager.claimDatabase;
      if (args.length >= 2)
        loadFrom = args[1].trim();

      Log.out("Loading claims from \"" + loadFrom + "\"...");
      if (!claims.load(loadFrom)) Log.err("Failed to load claims from the specified file.");
      else Log.out("Database loaded.");
    } else Log.out(Server.usage);

    return null;
  }

  /********************************************************************************************************
   * PAC Server Thread
   ****************************************************************************************************/

  public class ServerThread extends Thread {

    private final ScheduledExecutorService scheduler           = Executors.newScheduledThreadPool(1);
    private ScheduledFuture<?>             heartbeat           = null;
    private Client                         client              = null;
    private Packet                         pending             = null;
    private String                         username            = null, groups = null, computer = null, firstName = null;
    private String                         lastIncomingMsgTime = "none";
    private int                            claimId             = -1;
    private int                            lastBeat            = 0;

    /********************************************************************
     * Thread Constructor
     ******************************************************************/
    public ServerThread(Socket socket) throws NullPointerException {

      try {
        client = new Client(port, socket);
      } catch (final Exception e) {
        Log.err("failed to connect a client");
        throw new NullPointerException();
      }

      if (socket == null) {
        Log.err("an incoming connection is invalid, terminating the connection");
        throw new NullPointerException();
      }

      Log.out("Connection from " + socket.getClientSocket().getInetAddress() + "...");

      heartbeat = scheduler.scheduleAtFixedRate(new ServerHeartbeat(this), 0, 1, TimeUnit.SECONDS);
    }

    /********************************************************************
     * Basic Functions For Client Data Access
     ******************************************************************/
    public void killThread() {
      disconnect();
    }

    public int claimId() {
      return claimId;
    }
    public String getUsername() {
      return username;
    }
    public String getGroups() {
      return groups;
    }

    public boolean ping() {
      return (client != null && client.isConnected());
    }

    public void setPendingData(Packet p) {
      synchronized(scheduler) { pending = p; }
    }

    public Packet getPendingData() { return pending; }

    public boolean detectServerAttack() {
      if (!Utils.isDebugMode()) return false;

      if (lastIncomingMsgTime.equals("none")) lastIncomingMsgTime = Utils.getTime();
      else {
        /*
         * if (isAttack(lastIncomingMsgTime)) { return true; }
         */
      }

      lastIncomingMsgTime = Utils.getTime();
      return false;
    }


    public void disconnect() {

      try {

        if (client != null)
          client.disconnect(true);
        client = null;
        if (heartbeat != null)
          heartbeat.cancel(true);
        if (scheduler != null)
          scheduler.shutdownNow();

      } catch (final Exception e) {
        Log.err("socket error during disconnect");
      }

      lastIncomingMsgTime = "none";
    }

    /********************************************************************
     * Thread Main
     ******************************************************************/
    @Override
    public void run() {
      String[] data = null;

      try {
        Packet p = client.receive();
        data = p.data.split(ClaimManager.claimPartSeparatorSymbol);
      } catch (final Exception e) {
        e.printStackTrace();
      } finally {
        if (data == null || data.length != 3) {
          Log.err("an invalid attempt was made to connect to the server\n");
          client.disconnect(true);
          return;
        }
      }

      username            = data[0].trim();
      computer            = data[1].trim();
      firstName           = data[2].trim();
      groups              = Utils.getGroups(username);
      lastIncomingMsgTime = "none";

      // bad username received
      while (groups == null) {
        try {
          Packet p = null;
          if (groups == null) p = new Packet(PACKET.INVALID);
          if (client.send(p) != 0) {
            client.disconnect(true);
            return;
          }
          p = client.receive();

        } catch (Exception e) {
          Log.err("Bad username \"" + username + "\" rejected.");
          client.disconnect(true);
          return;
        }
      }

      // connection accepted
      try {

        synchronized(scheduler) {
          if (pending == null) pending = new Packet(PACKET.HEARTBEAT);

          if (client.send(pending) != 0) {
            client.disconnect(true);
            return;
          }
        }

      } catch (Exception e) {
        client.disconnect(true);
        return;
      }

      // obtain the users claims and send back
      try {
        Packet p = null;
        final ArrayList<Claim> claimsByUser = claims.getClaimsByUsername(username);
        Claim restoreClaim = null;
        if (claimsByUser != null) {
          for (final Claim c : claimsByUser) {
            if (c.username.equals(username) && c.state() != CLAIM_STATE.CANCELED &&
                c.state() != CLAIM_STATE.RESOLVED && c.state() != CLAIM_STATE.ERROR) {
              restoreClaim = c;
              Log.out("Restored claim " + c.id() + " to user \"" + c.username + "\".");
              break;
            }
          }
        }

        if (restoreClaim != null) {
          claimId = restoreClaim.id();
          p = new Packet(PACKET.CLAIM, restoreClaim.toString());
        } else
          p = new Packet(PACKET.HEARTBEAT); // just send a heartbeat if no claims are there

        try {
          if (client.send(p) != 0) {
            Log.err("failed to reestablish connection with \"" + username + "\".");
            client.disconnect(true);
            return;
          }
        } catch (final Exception e) {
          Log.err("failed to reestablish connection with \"" + username + "\".");
          client.disconnect(true);
          return;
        }

        if (restoreClaim != null) Log.out("\"" + username + "\" has reconnected to the server.");
        else Log.out("\"" + username + "\" has connected to the server.");

      } catch (final Exception e) {
        Log.err("a client has failed to connect properly, terminating connection");
        client.disconnect(true);
        return;
      }

      // **********************************************************************
      // begin infinite client loop:
      while (true) {
        try {
          Packet packet = client.receive();

          lastBeat = 0; // reset heartbeat counter immediately after

          // receiving transmission
          if (packet == null) {
            client.disconnect(true);
            return;
          }

          if (packet.is(PACKET.HEARTBEAT)) {

            continue;

          } else if (packet.is(PACKET.CLAIM)) {

            claimId = claims.add(packet.getData(), firstName,
                username, groups, computer, Utils.getTimeAndDate(),
                "Unresolved",
                CLAIM_STATE.UNRESOLVED.toString(), null);
            Utils.setConfigValue(CONFIG_VAR.CURRENT_CLAIM_ID, "" + (claimId + 1));
            log("Claim Opened");
            claims.save();

          } else if (packet.is(PACKET.RESOLVED)) try {
            final String consultant = packet.getData();
            final Claim c = claims.getClaimById(claimId);
            if (c.state() == CLAIM_STATE.RESOLVED) {
              Log.out("A request to resolve claim "
                  + claimId + " with consultant \"" + consultant
                  + "\" was received, but the claim is already resolved by "
                  + "\"" + c.consultant + "\".");
              return;
            }
            claims.resolve(claimId, consultant);
            log("Claim Resolved");
          } catch (final Exception e) {
            Log.err("failed to resolve claim with id " + claimId);
          }
          else if (packet.is(PACKET.HELPING)) {

            final String consultant = packet.getData();
            if (consultant == null) Log.out("A helping update was received for claim " + claimId
                + " with no consultant attached.");
            else {
              claims.editClaimState(claimId, CLAIM_STATE.HELPING);
              claims.attachConsultant(claimId, consultant);
              claims.save();
              log("Claim Review");
            }

          } else if (packet.is(PACKET.EDIT)) {

            final String edit = packet.getData();
            try {
              claims.editClaimString(claimId, edit);
              log("Claim Edited");
              claims.save();
            } catch (final Exception e) {
              Log.err("null edit received");
            }

          } else if (packet.is(PACKET.CANCEL)) try {
            final Claim c = claims.getClaimById(claimId);
            if (c == null) Log.out("Cannot cancel claim " + claimId + " because it does not exist.");
            else {
              claims.cancel(claimId);
              log("Claim Canceled");
              claims.save();
            }
          } catch (final Exception e) {
            Log.err("error canceling claim " + claimId);
          }
          else if (packet.is(PACKET.UNRESOLVED)) try {
            final Claim c = claims.getClaimById(claimId);
            if (c == null) Log.out("Cannot modify claim " + claimId + " because it does not exist.");
            else {
              claims.editClaimState(claimId, CLAIM_STATE.UNRESOLVED);
              claims.save();
              log("Claim Unresolved");
            }
          } catch (final Exception e) {
            Log.err("error modifying claim " + claimId);
          }
          else if (packet.is(PACKET.GET_CLAIMS)) try {

            final Packet temp = new Packet(PACKET.GET_CLAIMS, claims.getFullClaimsAsString());
            client.send(temp);

          } catch (final Exception e) {
            Log.err("failed sending the full claims list to remote client with username \"" + username + "\"");
          }
          else if (packet.is(PACKET.DISCONNECT)) {

            Log.out("\"" + username + "\" has disconnected.");
            client.disconnect();
            return;

          } else if (packet.is(PACKET.DATE_TIME)) try {
            final Packet temp = new Packet(PACKET.DATE_TIME, Utils.getTimeAndDate());
            client.send(temp);
          } catch (final Exception e) {
            Log.err("failed sending the date and time to remote client with username \"" + username + "\"");
          }
        } catch (final StringIndexOutOfBoundsException e) {

          Log.out("An error occured while interpreting a packet from username \"" + username + "\".");

        } catch (final Exception e) {

          Log.out("An error occured while receiving a packet from username \"" + username + "\".");

        }

      }
    }

    private void log(final String msg) {

      try {
        Log.printf(getConsoleFormat(5),
            msg, username, groups, computer, claims.getClaimById(claimId).consultant);
      } catch (final Exception e) {
        Log.err("failure retrieving claim with id " + claimId + " from the database");
      }

    }
  }

  /**************************************************************************************************************
   * Server Heartbeat
   **********************************************************************************************************/

  class ServerHeartbeat implements Runnable {
    final Integer heartbeat = Utils.getHeartbeat();
    ServerThread  client;                          // Named client because it refers to a remote

    // client connection
    ServerHeartbeat(final ServerThread client) {
      this.client = client;
    }

    /*
     * Every X seconds (see config file) a beat goes off.
     * If too many have passed with no beat, disconnect client.
     */
    @Override
    public void run() {
      try {
        if (client == null) {
          Log.debugLog("a null heartbeat thread is still ticking");
          return;
        }

        if (client.lastBeat > heartbeat) // Disconnect the client, no heartbeat
        {
          Log.out("Client \"" + client.getUsername()
              + "\" has no heartbeat, disconnecting.");
          client.disconnect();
          client = null;
          return;
        }
        client.lastBeat++;
      } catch (final Exception e) {
        Log.err("an unknown exception occured");
      }
    }
  }

  /**************************************************************************************************************
   * Main - Server
   **********************************************************************************************************/

  public static void main(final String[] args) {
    Log.out("PACServer (v" + HelpApp.VERSION + ") initializing...");

    if (!Utils.loadConfig()) if (!Utils.updateConfigFile()) { // create/load new config with defaults
      Log.err("fatal: cannot read/write a config file");
      System.exit(-1);
    }

    if (!Log.createLog(Utils.getServerLog())) {
      Log.err("fatal: cannot read/write a log file");
      System.exit(-1);
    }

    // Interpret port from commandline, if specified
    int port = Utils.getPort();
    if (args.length > 1) {
      final int tempPort = Server.portFromArgs(args);
      if (tempPort != -1)
        port = tempPort;
      if (!Utils.checkPort(port)) {
        Log.err("specified port " + Utils.getPort() + " is out of range, using default");
        port = Utils.getDefaultPort();
      }
    }

    // Create the server object and overwrite the default port with the one
    // specified on the commandline
    Server server = Server.createServer(port);
    if (server == null) {
      Log.err("fatal: failed to instantiate the server");
      Log.closeLog();
      System.exit(-1);
    }

    // prepare for input infinite loop
    String cmd = "";
    BufferedReader br = null;
    try {
      br = new BufferedReader(new InputStreamReader(System.in));
    } catch (final Exception e) {
      Log.err("failed to create an input stream");
    } finally {
      if (br == null) return;
    }

    // lame, fix: pause so the server has time to start before printing a '>'
    Utils.sleep(500);

    while (!cmd.equals("q")) {
      Server newServer = null;

      try {
        cmd = br.readLine().trim();
      } catch (final Exception e) {
        Log.err("bad input received");
        break;
      }

      try {
        final String[] temp = cmd.split(" ");
        if (!temp[0].equals("q"))
          newServer = server.interpretArgs(temp, server); // some commands may generate a new server
      } catch (final Exception e) {
        Log.out(Server.usage);
      }

      if (newServer != null) {
        server = newServer;
        Utils.sleep(500);
      }

    }

    if (server != null && server.listening) {
      Log.out("Stopping the PAC Server...");
      server.stopServer();
      try { server.join(); } catch (final Exception e) {}
    }

    if (!Utils.isDebugMode()) {

      server.save(); // save/backup database
      Utils.setConfigValue(CONFIG_VAR.DATABASE, ClaimManager.claimDatabase); // update with last database name used
      Log.out("Updating configuration file...");
      if (!Utils.updateConfigFile()) Log.err("failed to update the config file");

    } else Log.out("Configuration/database not saved because debug mode is on.");

    Log.out("The PAC Server has terminated properly.");
    Log.closeLog();
  }

}
